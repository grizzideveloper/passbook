package grizzi.esse3uniba.Utils;

import android.content.Context;
import android.content.pm.PackageInfo;

import com.android.volley.AuthFailureError;
import com.android.volley.BuildConfig;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

public class CrashReport {

    private static CrashReport instance;

    private static Context context;
    private static RequestQueue queue;
    private static final String baseUrl = "http://myreport.altervista.org/";
    private static final String instanceUrl = baseUrl + "open_instance.php";
    private static final String traceUrl = baseUrl + "trace_report.php";
    private static final String exceptionUrl = baseUrl + "report_exception.php";

    private static String instanceId = "";

    private CrashReport(Context context) {
        CrashReport.context = context;
        CrashReport.queue = Volley.newRequestQueue(context);
    }

    public static void init(Context context) {
        if(instance ==  null)instance = new CrashReport(context);
        openInstance();
    }

    private static void openInstance(){
        if(context != null){
            StringRequest stringRequest = new StringRequest(Request.Method.POST,instanceUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            instanceId = response;
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {

                        }
                    }
            ){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> params = new HashMap<>();
                    String versionName = "None";
                    try {
                        PackageInfo pinfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
                        int versionNumber = pinfo.versionCode;
                        versionName = pinfo.versionName + "_" + versionNumber;
                    }catch (Exception e){

                    }
                    params.put("Versione",versionName);
                    return params;
                }
            };
// Add the request to the RequestQueue.
            queue.add(stringRequest);
        }
    }

    public static void reportException(Throwable t){
        reportException(t,"");
    }

    public static void reportException(final Throwable t, final String dettagli){
        if(context != null){
// Request a string response from the provided URL.
            StringRequest stringRequest = new StringRequest(Request.Method.POST,exceptionUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                        }
                    }
            ){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> params = new HashMap<>();
                    params.put("Message",t.getMessage());
                    params.put("StackTrace",convertStackToString(t));
                    params.put("Dettagli",dettagli);
                    params.put("Versione","" + BuildConfig.VERSION_CODE);
                    return params;
                }
            };
// Add the request to the RequestQueue.
            queue.add(stringRequest);
        }
    }

    public static void reportTrace(final String tracciatura){
        if(context != null){
            StringRequest stringRequest = new StringRequest(Request.Method.POST,traceUrl,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                        }
                    }
            ){
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String,String> params = new HashMap<>();
                    params.put("Tracciatura",tracciatura);
                    return params;
                }
            };
            queue.add(stringRequest);
        }
    }

    private static String convertStackToString(Throwable throwable){
        StringWriter sw = new StringWriter();
        throwable.printStackTrace(new PrintWriter(sw));
        return sw.toString();
    }
}
