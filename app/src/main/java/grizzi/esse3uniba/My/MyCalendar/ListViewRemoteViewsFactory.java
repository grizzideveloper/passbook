package grizzi.esse3uniba.My.MyCalendar;

import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import java.util.ArrayList;

import grizzi.esse3uniba.My.Event;
import grizzi.esse3uniba.R;

/**
 * Created by grazi on 28/03/2017.
 */

class ListViewRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory {

    private Context mContext;

    private ArrayList<Event> records;


    public ListViewRemoteViewsFactory(Context context, Intent intent) {

        records = MyWidgetProvider.records;

        mContext = context;

    }

    // Initialize the data set.

    public void onCreate() {

        // In onCreate() you set up any connections / cursors to your data source. Heavy lifting,

        // for example downloading or creating content etc, should be deferred to onDataSetChanged()

        // or getViewAt(). Taking more than 20 seconds in this call will result in an ANR.

        //records=new ArrayList<String>();


    }

    // Given the position (index) of a WidgetItem in the array, use the item's text value in

    // combination with the app widget item XML file to construct a RemoteViews object.

    public RemoteViews getViewAt(int position) {

        // position will always range from 0 to getCount() - 1.

        // Construct a RemoteViews item based on the app widget item XML file, and set the

        // text based on the position.

        RemoteViews rv = new RemoteViews(mContext.getPackageName(), R.layout.widget_item);

        // feed row

        Event data=records.get(position);

        rv.setTextViewText(R.id.item, data.getName());
        rv.setTextViewText(R.id.itemH,data.getStartingHour() + "-" + data.getFinishHour());
        return rv;

    }

    public int getCount(){

        return records.size();

    }

    public void onDataSetChanged(){



    }

    public int getViewTypeCount(){

        return 1;

    }

    public long getItemId(int position) {

        return position;

    }

    public void onDestroy(){
        if(records != null)records.clear();
    }

    public boolean hasStableIds() {

        return true;

    }

    public RemoteViews getLoadingView() {

        return null;

    }

}