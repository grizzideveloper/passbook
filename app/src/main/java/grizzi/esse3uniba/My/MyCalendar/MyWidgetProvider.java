package grizzi.esse3uniba.My.MyCalendar;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.widget.RemoteViews;

import java.util.ArrayList;

import grizzi.esse3uniba.My.Event;
import grizzi.esse3uniba.Page.AddEventActivity;
import grizzi.esse3uniba.Page.WidgetCalendarActivity;
import grizzi.esse3uniba.R;

public class MyWidgetProvider extends AppWidgetProvider {


    private static Intent intent;
    public static ArrayList<Event> records = new ArrayList<Event>();;
    private MyCalendar calendar = MyCalendar.getCalendar();


    @Override
  public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {

        records = new ArrayList<Event>();
        ComponentName thisWidget = new ComponentName(context,
        MyWidgetProvider.class);
        int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
        for (int widgetId : allWidgetIds) {

            RemoteViews remoteViews = new RemoteViews(context.getPackageName(),
                R.layout.widget_layout);

            Intent intent = new Intent(context, ListViewWidgetService.class);
            intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
            intent.setData(Uri.parse(intent.toUri(Intent.URI_INTENT_SCHEME)));

            records.clear();
            records.addAll(calendar.getDayCalendar(0));

            remoteViews.setRemoteAdapter(R.id.listaLezioni, intent);

            //appWidgetManager.notifyAppWidgetViewDataChanged(widgetId,R.id.listaLezioni);
            //remoteViews.setEmptyView(android.R.id.list, R.id.tvEmptyList);

            // Set the text
            //remoteViews.setTextViewText(R.id.update, String.valueOf(number));
            setAllColor(remoteViews);
            remoteViews.setTextColor(R.id.lunText, Color.parseColor("#FFFFFFFF"));

            // Register an onClickListener

            //remoteViews.setRemoteAdapter();

            remoteViews.setOnClickPendingIntent(R.id.lunText, getPendingSelfIntent(context,"lun"));
            remoteViews.setOnClickPendingIntent(R.id.marText, getPendingSelfIntent(context,"mar"));
            remoteViews.setOnClickPendingIntent(R.id.merText, getPendingSelfIntent(context,"mer"));
            remoteViews.setOnClickPendingIntent(R.id.gioText, getPendingSelfIntent(context,"gio"));
            remoteViews.setOnClickPendingIntent(R.id.venText, getPendingSelfIntent(context,"ven"));
            intent = new Intent(context, WidgetCalendarActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
// Get the layout for the App Widget and attach an on-click listener to the button

            remoteViews.setOnClickPendingIntent(R.id.add,pendingIntent);
            appWidgetManager.updateAppWidget(widgetId, remoteViews);
        }
  }

   @Override
    public void onReceive(Context context, Intent intent) {
        this.intent = intent;
       ComponentName thisWidget = new ComponentName(context,
               MyWidgetProvider.class);
       RemoteViews control = new RemoteViews(context.getPackageName(),
               R.layout.widget_layout);
       AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(context);
       int[] allWidgetIds = appWidgetManager.getAppWidgetIds(thisWidget);
       calendar = MyCalendar.getCalendar();
       for (int widgetId : allWidgetIds) {
           switch(intent.getAction()){
               case "lun":
                   setAllColor(control);
                   control.setTextColor(R.id.lunText, Color.parseColor("#FFFFFFFF"));
                   records.clear();
                   records.addAll(calendar.getDayCalendar(0));
                   appWidgetManager.notifyAppWidgetViewDataChanged(widgetId,R.id.listaLezioni);
                   break;
               case "mar":
                   setAllColor(control);
                   control.setTextColor(R.id.marText, Color.parseColor("#FFFFFFFF"));
                   records.clear();
                   records.addAll(calendar.getDayCalendar(1));
                   appWidgetManager.notifyAppWidgetViewDataChanged(widgetId,R.id.listaLezioni);
                   break;
               case "mer":
                   setAllColor(control);
                   control.setTextColor(R.id.merText, Color.parseColor("#FFFFFFFF"));
                   records.clear();
                   records.addAll(calendar.getDayCalendar(2));
                   appWidgetManager.notifyAppWidgetViewDataChanged(widgetId,R.id.listaLezioni);
                   break;
               case "gio":
                   setAllColor(control);
                   control.setTextColor(R.id.gioText, Color.parseColor("#FFFFFFFF"));
                   records.clear();
                   records.addAll(calendar.getDayCalendar(3));
                   appWidgetManager.notifyAppWidgetViewDataChanged(widgetId,R.id.listaLezioni);
                   break;
               case "ven":
                   setAllColor(control);
                   control.setTextColor(R.id.venText, Color.parseColor("#FFFFFFFF"));
                   records.clear();
                   records.addAll(calendar.getDayCalendar(4));
                   appWidgetManager.notifyAppWidgetViewDataChanged(widgetId,R.id.listaLezioni);
                   break;
           }
           ComponentName cn = new ComponentName(context,
                   MyWidgetProvider.class);
           AppWidgetManager.getInstance(context).updateAppWidget(cn,
                   control);

       }
        super.onReceive(context,intent);
    }

    private void setAllColor(RemoteViews remoteViews){
        remoteViews.setTextColor(R.id.lunText, Color.parseColor("#808080"));
        remoteViews.setTextColor(R.id.marText, Color.parseColor("#808080"));
        remoteViews.setTextColor(R.id.merText, Color.parseColor("#808080"));
        remoteViews.setTextColor(R.id.gioText, Color.parseColor("#808080"));
        remoteViews.setTextColor(R.id.venText, Color.parseColor("#808080"));

    }

    protected PendingIntent getPendingSelfIntent(Context context, String action, int widgetId) {

        Intent intent = new Intent(context, MyWidgetProvider.class);
        intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
        intent.putExtra("day",action);
        return PendingIntent.getBroadcast(context,
                0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }


    protected PendingIntent getPendingSelfIntent(Context context, String action) {
        Intent intent = new Intent(context, getClass());
        intent.setAction(action);
        return PendingIntent.getBroadcast(context, 0, intent, 0);
    }

}
