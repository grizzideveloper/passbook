package grizzi.esse3uniba.My.MyCalendar;
import android.os.Environment;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.LinkedList;

import grizzi.esse3uniba.My.Event;


/**
 * Created by Graziano on 15/05/2016.
 */
public class  MyCalendar implements Serializable{

    private String days[] = {"Lunedì","Martedì","Mercoledì","Giovedì","Venerdì"};
    private LinkedList<Event> orario[] = new LinkedList[days.length];
    private String root;

    private static MyCalendar instance;

    private MyCalendar(){
        root = Environment.getExternalStorageDirectory().toString();

        try{
            load();
        }catch (FileNotFoundException e){
            for(int i =0;i < getNumberOfDays();i++){
                orario[i] = new LinkedList<Event>();
            }
            try{
                save();
            }catch (IOException e1){
                System.out.println("ERRORE SALVATAGGIO " + e1.getMessage().toString());
            }
        }catch (IOException e1){
            System.out.println("ERRORE LOAD" + e1.getMessage());
            for(int i =0;i < getNumberOfDays();i++){
                orario[i] = new LinkedList<Event>();
            }
            try{
                save();
            }catch (IOException e2){
                System.out.println("ERRORE SALVATAGGIO " + e1.getMessage().toString());
            }

        }catch (ClassNotFoundException e2){
            System.out.println("ERRORE CLASSE NN TROVATA");
        }
    }

    public static  MyCalendar getCalendar(){
        if(instance == null){
            instance = new MyCalendar();
        }
        return instance;
    }

    public int getNumberOfDays(){
        return days.length;
    }

    public LinkedList<Event> getDayCalendar(int dayNumber){
        return orario[dayNumber];
    }

    public void save()throws IOException{
        FileOutputStream file = new FileOutputStream(root + "/orario.dat");
        ObjectOutputStream writer = new ObjectOutputStream(file);
        writer.writeObject(getOrario());
        writer.close();
    }

    public void saveAll(){
        try {
            save();
        }catch (Exception e){

        }
    }

    private void load()throws FileNotFoundException,IOException,ClassNotFoundException{
        FileInputStream file = new FileInputStream(root + "/orario.dat");
        ObjectInputStream reader = new ObjectInputStream(file);
        setOrario((LinkedList<Event>[]) reader.readObject());
        reader.close();
    }


    private LinkedList<Event>[] getOrario(){
        return this.orario;
    }

    private void setOrario(LinkedList<Event>[] orario){
        this.orario = orario;
    }

    public void addEvent(int day,Event event){
        LinkedList eventList = getDayCalendar(day);
        boolean founded = false;

        for(int i = 0;i < eventList.size() && !founded;i++){
            if(event.compareTo(eventList.get(i)) == 1){
                eventList.add(i,event);
                founded = true;
            }
        }

        if(!founded){
            eventList.add(event);
        }
        try{
            save();
        }catch (IOException e){
            System.out.println("ERRORE SALVATAGGIO " + e.getMessage());
        }
    }

    public void clear(){
        for(int i = 0;i < orario.length;i++){
            orario[i].clear();
        }
        try {
            save();
        }catch (Exception  e){

        }
    }

}
