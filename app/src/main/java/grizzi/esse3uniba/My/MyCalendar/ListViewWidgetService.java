package grizzi.esse3uniba.My.MyCalendar;

import android.content.Intent;
import android.widget.RemoteViewsService;

/**
 * Created by grazi on 28/03/2017.
 */
public class ListViewWidgetService extends RemoteViewsService {



    public RemoteViewsService.RemoteViewsFactory onGetViewFactory(Intent intent) {

        return new ListViewRemoteViewsFactory(this.getApplicationContext(), intent);

    }

}