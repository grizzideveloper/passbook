package grizzi.esse3uniba.My;

import android.app.Application;


import java.util.HashMap;

import grizzi.esse3uniba.Page.Data.Data;
import grizzi.esse3uniba.Page.Data.DataAppelli;
import grizzi.esse3uniba.Page.Data.DataLibretto;
import grizzi.esse3uniba.Page.Data.DataPagamenti;
import grizzi.esse3uniba.Page.Data.DataPrenotazioni;
import grizzi.esse3uniba.Page.Data.DataProveParziali;
import grizzi.esse3uniba.Page.Data.DataVoti;
import grizzi.esse3uniba.Utils.CrashReport;


/**
 * Created by Graziano on 03/03/2016.
 */
public class MyApplication extends Application {

    private static MyApplication singleton;

    private DataAppelli dataPoolAppelli = null;
    private DataProveParziali dataPoolProveParziali = null;
    private DataLibretto dataPoolLibretto = null;
    private DataPrenotazioni dataPoolPrenotazioni = null;
    private DataVoti dataPoolVoti = null;
    private DataPagamenti dataPoolPagamenti = null;

    private HashMap<String,Data> cache = new HashMap<>();

    public Data getCache(String Url){
        return cache.get(Url);
    }

    public void saveCache(String Url,Data data){
        cache.put(Url,data);
    }

    public void deleteCache(String Url){
        cache.remove(Url);
    }

    public void clearCache(){
        cache.clear();
    }

    public static MyApplication getInstance(){
        return singleton;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;
        Thread.setDefaultUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
            @Override
            public void uncaughtException(Thread t, Throwable e) {
                CrashReport.reportException(e);
            }
        });
        CrashReport.init(this);
    }




/*
    public DataAppelli getDataPoolAppelli() {
        return dataPoolAppelli;
    }

    public void setDataPoolAppelli(DataAppelli dataPoolAppelli) {
        this.dataPoolAppelli = dataPoolAppelli;
    }

    public Boolean appelliIsVoid(){
        Boolean result = false;
        if (dataPoolAppelli == null)result = true;
        return result;
    }

    public DataProveParziali getDataPoolProveParziali() {
        return dataPoolProveParziali;
    }

    public void setDataPoolProveParziali(DataProveParziali dataPoolProveParziali) {
        this.dataPoolProveParziali = dataPoolProveParziali;
    }

    public Boolean proveParzialiIsVoid(){
        Boolean result = false;
        if (dataPoolProveParziali == null)result = true;
        return result;
    }



    public DataLibretto getDataPoolLibretto() {
        return dataPoolLibretto;
    }

    public void setDataPoolLibretto(DataLibretto dataPoolLibretto) {
        this.dataPoolLibretto = dataPoolLibretto;
    }

    public Boolean librettoIsVoid(){
        Boolean result = false;
        if (dataPoolLibretto == null)result = true;
        return result;
    }

    public DataPrenotazioni getDataPoolPrenotazioni() {
        return dataPoolPrenotazioni;
    }

    public void setDataPoolPrenotazioni(DataPrenotazioni dataPoolPrenotazioni) {
        this.dataPoolPrenotazioni = dataPoolPrenotazioni;
    }

    public Boolean prenotazioniIsVoid(){
        Boolean result = false;
        if (dataPoolPrenotazioni == null)result = true;
        return result;
    }

    public DataVoti getDataPoolVoti() {
        return dataPoolVoti;
    }

    public void setDataPoolVoti(DataVoti dataPoolVoti) {
        this.dataPoolVoti = dataPoolVoti;
    }

    public Boolean votiIsVoid(){
        Boolean result = false;
        if (dataPoolVoti == null)result = true;
        return result;
    }

    public DataPagamenti getDataPoolPagamenti() {
        return dataPoolPagamenti;
    }

    public void setDataPoolPagamenti(DataPagamenti dataPoolPagamenti) {
        this.dataPoolPagamenti = dataPoolPagamenti;
    }

    public Boolean pagamentiIsVoid(){
        Boolean result = false;
        if (dataPoolPagamenti == null)result = true;
        return result;
    }*/
}