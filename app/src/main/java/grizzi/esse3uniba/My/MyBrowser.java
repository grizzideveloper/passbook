package grizzi.esse3uniba.My;

import android.webkit.HttpAuthHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by Graziano Rizzi on 01/12/2016.
 */

public class MyBrowser extends WebViewClient {

    private String nick;
    private String password;

    public MyBrowser(String nick,String password){
        this.nick = nick;
        this.password = password;
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        view.loadUrl(url);
        return true;
    }

    @Override
    public void onReceivedHttpAuthRequest(WebView view,
                                          HttpAuthHandler handler, String host, String realm) {
        handler.proceed(nick, password);
    }

    @Override
    public void	onPageFinished(WebView view, String url){

        view.loadUrl("javascript:document.getElementById(\"testata_auth\").setAttribute(\"style\",\"display:none;\");");
        view.loadUrl("javascript:document.getElementById(\"menu-principale\").setAttribute(\"style\",\"display:none;\");");
        view.loadUrl("javascript:document.getElementById(\"barra-generale\").setAttribute(\"style\",\"display:none;\");");
        view.loadUrl("javascript:document.getElementById(\"footer\").setAttribute(\"style\",\"display:none;\");");
        //view.loadUrl("javascript:document.getElementById(\"main\").setAttribute(\"style\",\"width:100px;\");");
        //view.loadUrl("javascript:document.getElementById(\"corpo\").setAttribute(\"style\",\"width:100px;\");");

        view.loadUrl("javascript:document.getElementById(\"esse3old\").setAttribute(\"style\",\"width:100%;\");");
        view.loadUrl("javascript:document.getElementById(\"esse3old\").setAttribute(\"style\",\"margin-left:30px;\");");

        //view.loadUrl("javascript:document.getElementById(\"esse3old\").setAttribute(\"style\",\"margin-right:30px;\");");
    }


    @Override
    public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
        System.out.println("Error: " + errorCode + " " + description + " " + failingUrl);
    }
}