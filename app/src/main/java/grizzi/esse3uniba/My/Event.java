package grizzi.esse3uniba.My;

import java.io.Serializable;

/**
 * Created by Graziano on 16/05/2016.
 */
public class Event implements Serializable{
    private String name;
    private String startingHour,finishHour;

    public Event(String name,String startingHour,String finishHour) {
        this.name = name;
        this.startingHour = startingHour;
        this.finishHour = finishHour;
    }


    public int compareTo(Object o){
        if(getStartingHour().compareTo(((Event)o).getStartingHour())>0){
            return 1;
        }else if(getStartingHour().compareTo(((Event)o).getStartingHour())<0){
            return -1;
        }else {
            return 0;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStartingHour() {
        return startingHour;
    }

    public void setStartingHour(String startingHour) {
        this.startingHour = startingHour;
    }

    public String getFinishHour() {
        return finishHour;
    }

    public void setFinishHour(String finishHour) {
        this.finishHour = finishHour;
    }
}
