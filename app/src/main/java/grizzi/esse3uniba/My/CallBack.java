package grizzi.esse3uniba.My;

/**
 * Created by grazi on 29/03/2017.
 */

public interface CallBack {
    public void onUpdate();
}
