package grizzi.esse3uniba.My;

import java.util.ArrayList;

/**
 * Created by grazi on 29/03/2017.
 */

public class CallBackList<T> extends ArrayList<T> {

    public boolean add(T obj,CallBack call){
        call.onUpdate();
        return super.add(obj);
    }
}
