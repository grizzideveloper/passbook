package grizzi.esse3uniba.Page;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import android.view.View;

import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import org.jsoup.nodes.Document;


import grizzi.esse3uniba.Esse3.User;
import grizzi.esse3uniba.Esse3.UserStore;
import grizzi.esse3uniba.Esse3.Esse3Connection;
import grizzi.esse3uniba.Esse3.LoginListener;
import grizzi.esse3uniba.R;


public class Login extends AppCompatActivity {


    Context context = this;
    Button loginButton;
    TextView nameText, passwordText;
    CheckBox checkSalvaDati;
    ProgressDialog ringProgressDialog;

    UserStore userStore;
    Esse3Connection connection  = Esse3Connection.getInstance();

    User user = null;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        loginButton = (Button) findViewById(R.id.loginButton);
        nameText = (TextView) findViewById(R.id.nick);
        passwordText = (TextView) findViewById(R.id.pass);
        checkSalvaDati = (CheckBox)findViewById(R.id.checkBox);

        userStore = UserStore.getInstance(this);


        try{
            user = userStore.getSavedUser();
            checkSalvaDati.setChecked(true);

            nameText.setText(user.getName());
            passwordText.setText(user.getPassword());

        }catch (UserStore.NoUserFound e){
            checkSalvaDati.setChecked(false);
        }


        passwordText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InputMethodManager inputMethodManager=(InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.toggleSoftInputFromWindow(passwordText.getApplicationWindowToken(), InputMethodManager.SHOW_FORCED, 0);
            }
        });



        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if(isOnline()){
                        user = new User(nameText.getText().toString(),passwordText.getText().toString());
                        ringProgressDialog = ProgressDialog.show(Login.this, "Attendere ...", "Accesso in corso ...", true);
                        connection.startConnection(user, new LoginListener() {

                            @Override
                            public void onLoginDone(Document document) {

                                if(checkSalvaDati.isChecked()){
                                    userStore.saveUser(user);
                                }else {
                                    userStore.saveUser(null);//questo cancella l'user
                                }
                                ringProgressDialog.dismiss();

                                if (document != null && document.toString().toLowerCase().contains("scegli carriera")) {
                                    //Start activity scegli carriera
                                    Intent i = new Intent(getBaseContext(), PageCarriere.class);
                                    startActivity(i);
                                } else if(document == null){
                                    printMsg("Errore nel caricamento della pagina", context, Toast.LENGTH_LONG);
                                } else {
                                    //start activity menu principale
                                    Intent i = new Intent(getBaseContext(), PageMenu.class);
                                    startActivity(i);
                                }

                            }

                            @Override
                            public void onLoginFailed(Exception e) {
                                ringProgressDialog.dismiss();
                                printMsg("Password o nickname errati", context, Toast.LENGTH_LONG);
                            }

                        });
                    }else {
                        Toast.makeText(Login.this,"Internet assente",Toast.LENGTH_LONG).show();
                    }

                }catch (User.EmptyFieldException e1){
                    Toast.makeText(Login.this,e1.getMessage(),Toast.LENGTH_LONG).show();

                }catch (Exception e) {
                    Toast.makeText(Login.this,e.getMessage(),Toast.LENGTH_LONG).show();
                    ringProgressDialog.dismiss();
                }

            }
        });
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Login Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://grizzi.esse3uniba/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "Login Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://grizzi.esse3uniba/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    public boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null &&
                cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }


}
