package grizzi.esse3uniba.Page;

/**
 * Created by Graziano on 03/02/2016.
 */

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.jsoup.nodes.Document;

import grizzi.esse3uniba.Esse3.Esse3Connection;
import grizzi.esse3uniba.Esse3.Esse3Url;
import grizzi.esse3uniba.My.MyApplication;
import grizzi.esse3uniba.Utils.Utils;
import grizzi.esse3uniba.Page.Data.DataPrenotazioni;
import grizzi.esse3uniba.Page.Data.Element.ElementPrenotazioni;
import grizzi.esse3uniba.R;

/**
 * Created by hp1 on 21-01-2015.
 */
public class PagePrenotazioni extends AppCompatActivity {

    DataPrenotazioni dataPoolPrenotazioni;
    ListView listaPrenotazioni;
    Esse3Connection connection = Esse3Connection.getInstance();
    TextView textNoPrenotazioni;
    ProgressBar progressBar;
    private Context context = this;
    private Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tab_1);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("Prenotazioni");
        toolbar.setTitleTextColor(0xFFFFFFFF);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        progressBar = (ProgressBar)findViewById(R.id.progressBar);


        dataPoolPrenotazioni = new DataPrenotazioni(this){
            @Override
            public void cancellaPrenotazione(final ElementPrenotazioni prenotazione){
                if(isOnline()){
                    if(prenotazione.getCancellabile()){
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                        builder1.setMessage("Cancellare prenotazione?");
                        builder1.setCancelable(true);

                        builder1.setPositiveButton(
                                "Si",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        deleteAndReload(prenotazione);
                                        dialog.cancel();
                                    }
                                });

                        builder1.setNegativeButton(
                                "No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();

                    }else {
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                        builder1.setMessage("Questa prenotazione non può più essere cancellata");
                        builder1.setCancelable(true);

                        builder1.setPositiveButton(
                                "Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }
                }else {
                    Toast.makeText(context,"Internet assente",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void stampDialog(ElementPrenotazioni prenotazione){
                if(isOnline()){
                    String message = new String();
                    message += "Data: " + prenotazione.getData() + "\n";
                    message += "Ora: " + prenotazione.getOra() + "\n";
                    message += "Edificio: " + prenotazione.getEdificio() + "\n";
                    message += "Aula: " + prenotazione.getAula() + "\n";
                    message += "Docenti: " + prenotazione.getDocenti() + "\n";
                    message += "Riservato: " + prenotazione.getRiservato() + "\n";
                    message += prenotazione.getPosizione();

                    AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
                    builder1.setTitle("Informazioni");
                    builder1.setMessage(message);
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }else {
                    Toast.makeText(context,"Internet assente",Toast.LENGTH_LONG).show();
                }
            }
        };

        setView();

        load();
    }

    private void setView(){
        textNoPrenotazioni = (TextView)findViewById(R.id.textNoPrenotazioni);
        listaPrenotazioni = (ListView)findViewById(R.id.listPrenotazioni);
        listaPrenotazioni.setAdapter(dataPoolPrenotazioni.getAdapter());

    }

    private void load(){

        connection.getPage(Esse3Url.BACHECA_PRENOTAZIONI, new PageListener() {
            @Override
            public void onPageLoadDone(Document document) {
                dataPoolPrenotazioni.load(document);
                progressBar.setVisibility(View.GONE);
                if (!dataPoolPrenotazioni.isEmpty()) textNoPrenotazioni.setText("");
                else textNoPrenotazioni.setText("Nessuna prenotazione");
            }

            @Override
            public void onPageLoadFailed(Exception e) {
                if (Utils.isOnline(context)) {
                    Toast.makeText(context, "Sessione scaduta", Toast.LENGTH_LONG).show();
                    connection.close();
                    Intent i = new Intent(context, LoginActivity.class);
                    startActivity(i);
                } else {
                    Toast.makeText(context, "Internet Assente", Toast.LENGTH_LONG).show();
                }
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void reload(){
        dataPoolPrenotazioni.erease();
        load();
    }

    public void deleteAndReload(final ElementPrenotazioni prenotazione){

        final ProgressDialog ringProgressDialog = ProgressDialog.show(context, "Attendere...", "Cancellazione ...", true);
        connection.getPage(Esse3Url.CONFERMA_CANCELLA_PRENOTAZIONE + prenotazione.getId(), new PageListener() {
            @Override
            public void onPageLoadDone(Document document) {

                connection.getPage(Esse3Url.CANCELLA_PRENOTAZIONI + prenotazione.getId(), new PageListener() {
                    @Override
                    public void onPageLoadDone(Document document) {
                        reload();
                        MyApplication app = MyApplication.getInstance();
                        app.deleteCache(Esse3Url.APPELLI_URL);
                        app.deleteCache(Esse3Url.PROVE_URL);
                        ringProgressDialog.dismiss();
                    }

                    @Override
                    public void onPageLoadFailed(Exception e) {
                        ringProgressDialog.dismiss();
                        if(((PageMenu)context).isOnline()){
                            Toast.makeText(context,"Sessione scaduta",Toast.LENGTH_LONG).show();
                            connection.close();
                            Intent i = new Intent(context, LoginActivity.class);
                            startActivity(i);
                        }else{
                            Toast.makeText(context,"Internet Assente",Toast.LENGTH_LONG).show();
                        }
                    }
                });

            }

            @Override
            public void onPageLoadFailed(Exception e) {
                if(((PageMenu)context).isOnline()){
                    Toast.makeText(context,"Sessione scaduta",Toast.LENGTH_LONG).show();
                    connection.close();
                    Intent i = new Intent(context, LoginActivity.class);
                    startActivity(i);
                }else{
                    Toast.makeText(context,"Internet Assente",Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    public final boolean isOnline() {//funzione da spostare in connection
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                        return true;
        }
        return false;
    }


    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_blanck, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }else if (item.getItemId() == android.R.id.home){
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
