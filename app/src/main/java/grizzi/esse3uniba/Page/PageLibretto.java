package grizzi.esse3uniba.Page;


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import com.github.florent37.materialviewpager.MaterialViewPager;
import com.github.florent37.materialviewpager.header.HeaderDesign;
import com.hookedonplay.decoviewlib.DecoView;
import com.hookedonplay.decoviewlib.charts.SeriesItem;

import org.jsoup.nodes.Document;

import butterknife.BindView;
import butterknife.ButterKnife;

import grizzi.esse3uniba.Esse3.Esse3Url;
import grizzi.esse3uniba.My.MyApplication;
import grizzi.esse3uniba.Page.Data.Data;
import grizzi.esse3uniba.R;
import grizzi.esse3uniba.Page.Data.DataLibretto;
import grizzi.esse3uniba.viewPager.fragment.RecyclerViewFragmentLibretto;


/**
 * Created by Graziano on 05/02/2016.
 */
public class PageLibretto extends SitePage<DataLibretto> {

    private DrawerLayout mDrawer;
    private ActionBarDrawerToggle mDrawerToggle;
    @BindView(R.id.materialViewPager)
    MaterialViewPager mViewPager;

    RecyclerViewFragmentLibretto recyclerViewFragmentLibretto = null;
    Toolbar toolbar;
    Document doc;
    Context context =  this;
    ListView listaLibretto;
    TextView aritmetica,ponderata;
    DecoView decoView, decoView2;
    View view;

    ProgressBar progressBar;

    MyApplication app;

    @Override
    protected String getUrl(){
        return Esse3Url.LIBRETTO_URL;
    }

    @Override
    protected Data initDataPool(){
        return new DataLibretto(context);
    }


    @Override
    protected void setView(){
        /*
        setContentView(R.layout.activity_libretto_new_new);

        progressBar = (ProgressBar)findViewById(R.id.progressBar);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("Libretto");
        toolbar.setTitleTextColor(0xFFFFFFFF);

        //aritmetica = (TextView)findViewById(R.id.textValoreAritmetica);
        //ponderata = (TextView)findViewById(R.id.textValorePonderata);

        listaLibretto = (ListView)findViewById(R.id.listLibretto);

        //decoView = (DecoView) findViewById(R.id.dynamicArcView);
        //decoView2 = (DecoView) findViewById(R.id.dynamicArcView2);

        SeriesItem seriesItem = new SeriesItem.Builder(Color.parseColor("#FFE2E2E2"))
                .setRange(0, 30, 30)
                .build();

        //decoView.addSeries(seriesItem);
        //decoView2.addSeries(seriesItem);


        view = findViewById(R.id.layCB);
        arcProgressStackView = (ArcProgressStackView) findViewById(R.id.apsv);


        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        listaLibretto.setAdapter(getDataPool().getAdapter());
        final ArrayList<ArcProgressStackView.Model> models = new ArrayList<>();

        models.add(new ArcProgressStackView.Model("Aritmetica", 0,30, Color.parseColor("#cccccc"), Color.parseColor("#26327f")));
        models.add(new ArcProgressStackView.Model("Pesata", 0,30, Color.parseColor("#dddddd"), Color.parseColor("#2b388f")));
        models.add(new ArcProgressStackView.Model("Crediti", 0,100, Color.parseColor("#eeeeee"), Color.parseColor("#303f9f")));
        arcProgressStackView.setModels(models);*/


        setContentView(R.layout.activity_main);
        setTitle("");
        ButterKnife.bind(this);

        final Toolbar toolbar = mViewPager.getToolbar();
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }

        mViewPager.getViewPager().setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager()) {

            @Override
            public Fragment getItem(int position) {
                switch (position % 4) {
                    //case 0:
                    //    return RecyclerViewFragment.newInstance();
                    //case 1:
                    //    return RecyclerViewFragment.newInstance();
                    //case 2:
                    //    return WebViewFragment.newInstance();
                    default:
                        recyclerViewFragmentLibretto = RecyclerViewFragmentLibretto.newInstance(getDataPool().getMedie(),getDataPool().getListaVoci());
                        return recyclerViewFragmentLibretto;
                }
            }

            @Override
            public int getCount() {
                return 1;
            }

            @Override
            public CharSequence getPageTitle(int position) {
                switch (position % 4) {
                    case 0:
                        return "Libretto";
                    case 1:
                        return "Actualités";
                    case 2:
                        return "Professionnel";
                    case 3:
                        return "Divertissement";
                }
                return "";
            }
        });

        mViewPager.setMaterialViewPagerListener(new MaterialViewPager.Listener() {
            @Override
            public HeaderDesign getHeaderDesign(int page) {
                switch (page) {
                    case 1:
                        return HeaderDesign.fromColorResAndUrl(
                                R.color.green,
                                "http://phandroid.s3.amazonaws.com/wp-content/uploads/2014/06/android_google_moutain_google_now_1920x1080_wallpaper_Wallpaper-HD_2560x1600_www.paperhi.com_-640x400.jpg");
                    case 0:
                        return HeaderDesign.fromColorResAndUrl(
                                R.color.blue,
                                "https://storage.googleapis.com/material-design/publish/material_v_11/assets/0Bx4BSt6jniD7Wm9Tcmx5d2tueUE/style_imagery_bestpractices_narrative1.png");
                    case 2:
                        return HeaderDesign.fromColorResAndUrl(
                                R.color.cyan,
                                "http://www.droid-life.com/wp-content/uploads/2014/10/lollipop-wallpapers10.jpg");
                    case 3:
                        return HeaderDesign.fromColorResAndUrl(
                                R.color.red,
                                "http://www.tothemobile.com/wp-content/uploads/2014/07/original.jpg");
                }

                //execute others actions if needed (ex : modify your header logo)

                return null;
            }
        });

        mViewPager.getViewPager().setOffscreenPageLimit(mViewPager.getViewPager().getAdapter().getCount());
        mViewPager.getPagerTitleStrip().setViewPager(mViewPager.getViewPager());

        final View logo = findViewById(R.id.logo_white);
        if (logo != null) {
            logo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mViewPager.notifyHeaderChanged();
                    Toast.makeText(getApplicationContext(), "Yes, the title is clickable", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    @Override
    public void onPageLoaded(){

        if(recyclerViewFragmentLibretto!=null)
        recyclerViewFragmentLibretto.update();
        //setMedie();
        //progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onPageLoadError(){
        super.onPageLoadError();
        //progressBar.setVisibility(View.GONE);
    }

    private void setMedie(){

        //aritmetica.setText("" + getDataPool().getmAritmetica());
        //ponderata.setText("" + getDataPool().getmPonderata());

        SeriesItem seriesVotoA = new SeriesItem.Builder(Color.parseColor("#3F51B5"))
                .setRange(0, 30,(int) getDataPool().getMediaAritmentica())
                .build();
        //decoView.addSeries(seriesVotoA);

        SeriesItem seriesVotoP = new SeriesItem.Builder(Color.parseColor("#3F51B5"))
                .setRange(0, 30,(int) getDataPool().getMediaPonderata())
                .build();
        //decoView2.addSeries(seriesVotoP);



        /*models.add(new ArcProgressStackView.Model("Aritmetica", aritmetica,30, Color.parseColor("#cccccc"), Color.parseColor("#26327f")));
        models.add(new ArcProgressStackView.Model("Pesata", (float)getDataPool().getMediaPonderata(),30, Color.parseColor("#dddddd"), Color.parseColor("#2b388f")));
        models.add(new ArcProgressStackView.Model("Crediti", 75,100, Color.parseColor("#eeeeee"), Color.parseColor("#303f9f")));
        arcProgressStackView.setModels(models);
        arcProgressStackView.invalidate();*/

    }

    @Override
    protected void onStart() {
        super.onStart();

        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawer, 0, 0);
        mDrawerToggle.setDrawerIndicatorEnabled(false);
        mDrawer.setDrawerListener(mDrawerToggle);

        final ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            /*
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setDisplayUseLogoEnabled(false);
            actionBar.setHomeButtonEnabled(true);*/
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

}
