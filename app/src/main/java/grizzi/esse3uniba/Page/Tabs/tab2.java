package grizzi.esse3uniba.Page.Tabs;

/**
 * Created by Graziano on 03/02/2016.
 */
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import grizzi.esse3uniba.Page.PageAppelli;
import grizzi.esse3uniba.Page.PageLibretto;
import grizzi.esse3uniba.Page.PagePagamenti;
import grizzi.esse3uniba.Page.PageProveParziali;
import grizzi.esse3uniba.R;

/**
 * Created by hp1 on 21-01-2015.
 */
public class tab2 extends Fragment {

    RelativeLayout libretto,appelli,prove,pagamenti;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tab_2,container,false);

        libretto = (RelativeLayout)v.findViewById(R.id.layoutLibretto);
        appelli = (RelativeLayout)v.findViewById(R.id.layoutAppelli);
        prove = (RelativeLayout)v.findViewById(R.id.layoutParziali);
        pagamenti = (RelativeLayout)v.findViewById(R.id.layoutPagamenti);

        libretto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(view.getContext(), PageLibretto.class);
                startActivity(i);
            }
        });

        appelli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(view.getContext(), PageAppelli.class);
                startActivity(i);
            }
        });

        prove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(view.getContext(), PageProveParziali.class);
                startActivity(i);
            }
        });

        pagamenti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(view.getContext(), PagePagamenti.class);
                startActivity(i);
            }
        });



        return v;
    }
}
