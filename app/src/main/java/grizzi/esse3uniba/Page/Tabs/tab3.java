package grizzi.esse3uniba.Page.Tabs;

/**
 * Created by Graziano on 03/02/2016.
 */
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.jsoup.nodes.Document;

import grizzi.esse3uniba.Esse3.Esse3Connection;
import grizzi.esse3uniba.Esse3.Esse3Url;
import grizzi.esse3uniba.My.MyWebView;
import grizzi.esse3uniba.Page.*;
import grizzi.esse3uniba.R;
import grizzi.esse3uniba.Page.Data.DataVoti;
import grizzi.esse3uniba.Page.Data.Element.ElementVoti;

/**
 * Created by hp1 on 21-01-2015.
 */
public class tab3 extends Fragment {

    ListView listVoti;
    TextView textNoVoti;
    ProgressBar progressBar;

    Button libretto,appelli,prove;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.tab_3,container,false);

        textNoVoti = (TextView)v.findViewById(R.id.textNoVoti);
        progressBar = (ProgressBar)v.findViewById(R.id.progressBar);

        final DataVoti dataPoolVoti = new DataVoti(getActivity().getBaseContext()){
            @Override
            public void onItemClick(ElementVoti voto){
                //Intent i = new Intent(getActivity().getBaseContext(), PageConfermaVoto.class);
                //i.putExtra("ID_ESAME", voto.getId());
                //startActivity(i);
                /*
                AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                builder1.setMessage("Mi dispiace, la convalida dei voti sarà inserita prossimamente");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "Ok",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {

                                dialog.cancel();
                            }
                        });


                AlertDialog alert11 = builder1.create();
                alert11.show();*/
                if(isOnline()){
                    Intent i = new Intent(v.getContext(), MyWebView.class);
                    i.putExtra("URL",Esse3Url.GESTIONE_VOTI + voto.getId() /*Da aggiungere l'id dell'esame voto.getId()*/);
                    startActivity(i);
                }else {
                    Toast.makeText(getView().getContext(),"Internet assente",Toast.LENGTH_LONG).show();
                }
            }
        };

        listVoti = (ListView)v.findViewById(R.id.listVoti);
        listVoti.setAdapter(dataPoolVoti.getAdapter());

        final Esse3Connection connection =  Esse3Connection.getInstance();
        connection.getPage(Esse3Url.BACHECA_VOTI, new PageListener() {
            @Override
            public void onPageLoadDone(Document document) {
                dataPoolVoti.load(document);
                progressBar.setVisibility(View.GONE);
                if(!dataPoolVoti.isEmpty())textNoVoti.setText("");
                else textNoVoti.setText("Nessun voto pendente");
            }

            @Override
            public void onPageLoadFailed(Exception e) {

                if(((PageMenu)getActivity()).isOnline()){
                    Toast.makeText(getView().getContext(), "Sessione scaduta", Toast.LENGTH_LONG).show();
                    connection.close();
                    Intent i = new Intent(getView().getContext(), LoginActivity.class);
                    startActivity(i);
                }else{
                    Toast.makeText(getView().getContext(),"Internet Assente",Toast.LENGTH_LONG).show();
                }
                progressBar.setVisibility(View.GONE);
            }
        });




        return v;
    }

    public final boolean isOnline() {//funzione da spostare in connection
        ConnectivityManager connectivity = (ConnectivityManager) getView().getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                        return true;
        }
        return false;
    }
}
