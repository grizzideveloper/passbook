package grizzi.esse3uniba.Page.Tabs;

/**
 * Created by Graziano on 03/02/2016.
 */

import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.LinkedList;
import java.util.List;

import grizzi.esse3uniba.My.Event;
import grizzi.esse3uniba.My.MyCalendar.MyCalendar;
import grizzi.esse3uniba.R;

/**
 * Created by hp1 on 21-01-2015.
 */
public class CalendarTab extends Fragment {
    private View v;
    private int dayNumber;
    private MyCalendar calendar = MyCalendar.getCalendar();
    private ListView listView;
    private LinkedList<Event> list;


    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.calendar_tab, container, false);
        listView = (ListView)v.findViewById(R.id.listCalendar);

        Bundle args = getArguments();
        dayNumber = args.getInt("dayNumber");
        list = calendar.getDayCalendar(dayNumber);



        CalendarAdapter adapter = new CalendarAdapter(getActivity(),0,list);
        listView.setAdapter(adapter);

        return v;
    }

    private void setView(){


    }


    public final boolean isOnline() {//funzione da spostare in connection
        ConnectivityManager connectivity = (ConnectivityManager) getView().getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                        return true;
        }
        return false;
    }


    private class CalendarAdapter extends ArrayAdapter<Event> {

        public CalendarAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        public CalendarAdapter(Context context, int resource, List<Event> items) {
            super(context, resource, items);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View v = convertView;

            if (v == null) {
                LayoutInflater vi;
                vi = LayoutInflater.from(getContext());
                v = vi.inflate(R.layout.calendar_row, null);
            }

            Event event = (Event)getItem(position);

            TextView name = (TextView)v.findViewById(R.id.textEvent);
            TextView startHour = (TextView)v.findViewById(R.id.textStartHour);
            TextView endHour  = (TextView)v.findViewById(R.id.textEndHour);

            name.setText(event.getName());
            startHour.setText(event.getStartingHour());
            endHour.setText(event.getFinishHour());

            ImageView delete = (ImageView)v.findViewById(R.id.delete);
            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                    builder1.setMessage("Cancellare?");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "Si",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    list.remove(position);
                                    try {
                                        calendar.save();
                                    }catch (Exception e){

                                    }
                                    notifyDataSetChanged();
                                    dialog.cancel();
                                }
                            });

                    builder1.setNegativeButton(
                            "No",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();

                }
            });

            return v;
        }

    }

}
