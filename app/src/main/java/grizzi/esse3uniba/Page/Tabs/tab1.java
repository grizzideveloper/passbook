package grizzi.esse3uniba.Page.Tabs;

/**
 * Created by Graziano on 03/02/2016.
 */
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.jsoup.nodes.Document;

import grizzi.esse3uniba.Esse3.Esse3Connection;
import grizzi.esse3uniba.Esse3.Esse3Url;
import grizzi.esse3uniba.My.MyApplication;
import grizzi.esse3uniba.Page.*;
import grizzi.esse3uniba.R;
import grizzi.esse3uniba.Page.Data.DataPrenotazioni;
import grizzi.esse3uniba.Page.Data.Element.ElementPrenotazioni;

/**
 * Created by hp1 on 21-01-2015.
 */
public class tab1 extends Fragment {
    int count = 0;
    DataPrenotazioni dataPoolPrenotazioni;
    ListView listaPrenotazioni;
    Esse3Connection connection = Esse3Connection.getInstance();
    TextView textNoPrenotazioni;
    View v;
    ProgressBar progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.tab_1, container, false);

        progressBar = (ProgressBar)v.findViewById(R.id.progressBar);


        dataPoolPrenotazioni = new DataPrenotazioni(getActivity().getBaseContext()){
            @Override
            public void cancellaPrenotazione(final ElementPrenotazioni prenotazione){
                if(isOnline()){
                    if(prenotazione.getCancellabile()){
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                        builder1.setMessage("Cancellare prenotazione?");
                        builder1.setCancelable(true);

                        builder1.setPositiveButton(
                                "Si",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        deleteAndReload(prenotazione);
                                        dialog.cancel();
                                    }
                                });

                        builder1.setNegativeButton(
                                "No",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();

                    }else {
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                        builder1.setMessage("Questa prenotazione non può più essere cancellata");
                        builder1.setCancelable(true);

                        builder1.setPositiveButton(
                                "Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }
                }else {
                    Toast.makeText(getView().getContext(),"Internet assente",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void stampDialog(ElementPrenotazioni prenotazione){
                if(isOnline()){
                    String message = new String();
                    message += "Data: " + prenotazione.getData() + "\n";
                    message += "Ora: " + prenotazione.getOra() + "\n";
                    message += "Edificio: " + prenotazione.getEdificio() + "\n";
                    message += "Aula: " + prenotazione.getAula() + "\n";
                    message += "Docenti: " + prenotazione.getDocenti() + "\n";
                    message += "Riservato: " + prenotazione.getRiservato() + "\n";
                    message += prenotazione.getPosizione();

                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                    builder1.setTitle("Informazioni");
                    builder1.setMessage(message);
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }else {
                    Toast.makeText(getView().getContext(),"Internet assente",Toast.LENGTH_LONG).show();
                }
            }
        };

        setView();

        load();

        return v;
    }

    private void setView(){
        textNoPrenotazioni = (TextView)v.findViewById(R.id.textNoPrenotazioni);
        listaPrenotazioni = (ListView)v.findViewById(R.id.listPrenotazioni);
        listaPrenotazioni.setAdapter(dataPoolPrenotazioni.getAdapter());

    }

    private void load(){

        connection.getPage(Esse3Url.BACHECA_PRENOTAZIONI, new PageListener() {
            @Override
            public void onPageLoadDone(Document document) {
                dataPoolPrenotazioni.load(document);
                progressBar.setVisibility(View.GONE);
                if (!dataPoolPrenotazioni.isEmpty()) textNoPrenotazioni.setText("");
                else textNoPrenotazioni.setText("Nessuna prenotazione");
            }

            @Override
            public void onPageLoadFailed(Exception e) {
                if (((PageMenu) getActivity()).isOnline()) {
                    Toast.makeText(getView().getContext(), "Sessione scaduta", Toast.LENGTH_LONG).show();
                    connection.close();
                    Intent i = new Intent(getView().getContext(), LoginActivity.class);
                    startActivity(i);
                } else {
                    Toast.makeText(getView().getContext(), "Internet Assente", Toast.LENGTH_LONG).show();
                }
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    private void reload(){
        dataPoolPrenotazioni.erease();
        load();
    }

    public void deleteAndReload(final ElementPrenotazioni prenotazione){

        final ProgressDialog ringProgressDialog = ProgressDialog.show(getActivity(), "Attendere...", "Cancellazione ...", true);
        connection.getPage(Esse3Url.CONFERMA_CANCELLA_PRENOTAZIONE + prenotazione.getId(), new PageListener() {
            @Override
            public void onPageLoadDone(Document document) {

                connection.getPage(Esse3Url.CANCELLA_PRENOTAZIONI + prenotazione.getId(), new PageListener() {
                    @Override
                    public void onPageLoadDone(Document document) {
                        reload();
                        MyApplication app = MyApplication.getInstance();
                        app.deleteCache(Esse3Url.APPELLI_URL);
                        app.deleteCache(Esse3Url.PROVE_URL);
                        ringProgressDialog.dismiss();
                    }

                    @Override
                    public void onPageLoadFailed(Exception e) {
                        ringProgressDialog.dismiss();
                        if(((PageMenu)getActivity()).isOnline()){
                            Toast.makeText(getView().getContext(),"Sessione scaduta",Toast.LENGTH_LONG).show();
                            connection.close();
                            Intent i = new Intent(getView().getContext(), LoginActivity.class);
                            startActivity(i);
                        }else{
                            Toast.makeText(getView().getContext(),"Internet Assente",Toast.LENGTH_LONG).show();
                        }
                    }
                });

            }

            @Override
            public void onPageLoadFailed(Exception e) {
                if(((PageMenu)getActivity()).isOnline()){
                    Toast.makeText(getView().getContext(),"Sessione scaduta",Toast.LENGTH_LONG).show();
                    connection.close();
                    Intent i = new Intent(getView().getContext(), LoginActivity.class);
                    startActivity(i);
                }else{
                    Toast.makeText(getView().getContext(),"Internet Assente",Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    public final boolean isOnline() {//funzione da spostare in connection
        ConnectivityManager connectivity = (ConnectivityManager) getView().getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                        return true;
        }
        return false;
    }
}
