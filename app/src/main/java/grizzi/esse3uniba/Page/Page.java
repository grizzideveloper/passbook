package grizzi.esse3uniba.Page;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import org.jsoup.nodes.Document;

import grizzi.esse3uniba.Esse3.Esse3Connection;
import grizzi.esse3uniba.Esse3.User;
import grizzi.esse3uniba.My.MyApplication;
import grizzi.esse3uniba.Page.Data.Data;
import grizzi.esse3uniba.R;

/**
 * Created by Graziano on 25/02/2016.
 */
public abstract class Page<T extends Data> extends AppCompatActivity {

    protected Esse3Connection connection = Esse3Connection.getInstance();
    protected Context context = this;
    private T dataPool;
    private boolean cacheValidity = false;
    private MyApplication app = MyApplication.getInstance();

    /////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////

    protected abstract String    getUrl();
    protected abstract void      setView();
    protected abstract Data      initDataPool();
    protected abstract void      onPageLoaded();
    protected abstract User      getUser();

    ////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////

    ///////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private final T castedDataPool(){
        return (T)initDataPool();
    }

    public final void setDataPool()throws NullPointerException{

        Data cache = app.getCache(getUrl());

        if(cache == null){
            this.dataPool = castedDataPool();
            setCacheValidity(false);
        }else {
            this.dataPool = (T)cache;//
            setCacheValidity(true);
        }
    }

    public final T getDataPool(){
        return dataPool;
    }

    public final boolean isOnline() {//funzione da spostare in connection
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                        return true;
        }
        return false;
    }

    public final void backToLogin(){
        Toast.makeText(context, "Sessione scaduta", Toast.LENGTH_LONG).show();
        connection.close();
        Intent i = new Intent(context, LoginActivity.class);
        startActivity(i);
        MyApplication.getInstance().clearCache();
        finish();
    }

    protected void load(){

        if(!isCacheValid()){
            connection.getPage(getUrl(), new PageListener() {

                @Override
                public void onPageLoadDone(Document document) {
                    getDataPool().load(document);
                    if(mustSaveCache())app.saveCache(getUrl(), getDataPool());
                    onPageLoaded();
                }

                @Override
                public void onPageLoadFailed(Exception e) {
                    onPageLoadError();
                }
            });
        }else {
            onPageLoaded();
        }

    }

    protected boolean mustSaveCache(){
        return true;
    }

    protected void refresh(){
        getDataPool().erease();
        load();
    }

    public void onPageLoadError(){
        if (isOnline()) {
            backToLogin();
        } else {
            Toast.makeText(context, "Internet Assente", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    protected boolean isCacheValid() {
        return cacheValidity;
    }

    protected void setCacheValidity(boolean cacheValidity) {
        this.cacheValidity = cacheValidity;
    }


    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_blanck, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }else if (item.getItemId() == android.R.id.home){
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

}
