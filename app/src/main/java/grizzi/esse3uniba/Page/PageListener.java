package grizzi.esse3uniba.Page;

import android.content.Context;
import android.os.Handler;
import android.widget.Toast;

import org.jsoup.nodes.Document;

/**
 * Created by Graziano on 02/02/2016.
 */
public abstract class PageListener {

    public abstract void onPageLoadDone(Document document);

    public abstract void onPageLoadFailed(Exception e);


    public void printMsg(final String msg,final Context context, final int length){
        Handler handler =  new Handler(context.getMainLooper());
        handler.post( new Runnable(){
            public void run(){
                Toast.makeText(context, msg, length).show();
            }
        });
    }
}
