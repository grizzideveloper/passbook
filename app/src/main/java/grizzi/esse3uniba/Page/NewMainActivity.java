package grizzi.esse3uniba.Page;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.View;
import com.google.android.material.navigation.NavigationView;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.jsoup.nodes.Document;

import grizzi.esse3uniba.Esse3.Esse3Url;
import grizzi.esse3uniba.Esse3.UserStore;
import grizzi.esse3uniba.My.MyApplication;
import grizzi.esse3uniba.My.MySharedPreferences;
import grizzi.esse3uniba.Page.Data.Data;
import grizzi.esse3uniba.Page.Data.DataMenu;
import grizzi.esse3uniba.R;

public class NewMainActivity extends SitePage<DataMenu>
        implements NavigationView.OnNavigationItemSelectedListener {

    private RelativeLayout libretto,appelli,prove,pagamenti;
    private TextView navText,matText;
    private Context context = this;
    private Toolbar toolbar;

    @Override
    protected String getUrl(){
        return Esse3Url.HOME_URL;
    }

    @Override
    protected Data initDataPool(){
        return new DataMenu(context);
    }


    @Override
    public void onPageLoaded(){
 //       matText.setText("Matricola - " + getDataPool().getMatricola());
//        navText.setText(getDataPool().getNome());
    }

    @Override
    public void onPageLoadError(){
        super.onPageLoadError();
    }


    @Override
    protected void setView() {
        setContentView(R.layout.activity_new_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        libretto = (RelativeLayout)findViewById(R.id.layoutLibretto);
        appelli = (RelativeLayout)findViewById(R.id.layoutAppelli);
        prove = (RelativeLayout)findViewById(R.id.layoutParziali);
        pagamenti = (RelativeLayout)findViewById(R.id.layoutPagamenti);

        libretto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(view.getContext(), PageLibretto.class);
                startActivity(i);
            }
        });

        appelli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(view.getContext(), PageAppelli.class);
                startActivity(i);
            }
        });

        prove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(view.getContext(), PageProveParziali.class);
                startActivity(i);
            }
        });

        pagamenti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(view.getContext(), PagePagamenti.class);
                startActivity(i);
            }
        });

        navText = (TextView) findViewById(R.id.navText);
        matText = (TextView) findViewById(R.id.matText);
        showAlert();
    }

    private void showAlert(){

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            moveTaskToBack(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add("Logout").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                if(isOnline()){
                    final ProgressDialog ringProgressDialog =  ProgressDialog.show(context, "Attendere ...", "Chiusua in corso ...", true);
                    connection.getPage("https://www.studenti.ict.uniba.it/esse3/Logout.do", new PageListener() {
                        @Override
                        public void onPageLoadDone(Document document) {
                            connection.stopConnection();
                            MyApplication app = MyApplication.getInstance();
                            app.clearCache();
                            UserStore uStore = UserStore.getInstance(context);
                            uStore.clear();
                            startActivity(new Intent(context,LoginActivity.class));
                            ringProgressDialog.dismiss();
                        }

                        @Override
                        public void onPageLoadFailed(Exception e) {
                            ringProgressDialog.dismiss();
                        }
                    });
                }else {
                    Toast.makeText(context,"Internet assente",Toast.LENGTH_LONG).show();
                }
                return false;
            }
        });
        return true;
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {
            Intent intent = new Intent(context,PagePrenotazioni.class);
            startActivity(intent);
        } else if (id == R.id.nav_gallery) {
            Intent intent = new Intent(context,PageEsiti.class);
            startActivity(intent);
        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_avvisi) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
