package grizzi.esse3uniba.Page;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.jsoup.nodes.Document;

import grizzi.esse3uniba.Esse3.Esse3Connection;
import grizzi.esse3uniba.Esse3.Esse3Url;
import grizzi.esse3uniba.My.MyWebView;
import grizzi.esse3uniba.Page.Data.DataVoti;
import grizzi.esse3uniba.Page.Data.Element.ElementVoti;
import grizzi.esse3uniba.R;

public class PageEsiti extends AppCompatActivity {

    private ListView listVoti;
    private TextView textNoVoti;
    private ProgressBar progressBar;
    private Toolbar toolbar;
    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tab_3);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("Prenotazioni");
        toolbar.setTitleTextColor(0xFFFFFFFF);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        textNoVoti = (TextView)findViewById(R.id.textNoVoti);
        progressBar = (ProgressBar)findViewById(R.id.progressBar);

        final DataVoti dataPoolVoti = new DataVoti(context){
            @Override
            public void onItemClick(ElementVoti voto){
                if(isOnline()){
                    Intent i = new Intent(context, MyWebView.class);
                    i.putExtra("URL", Esse3Url.GESTIONE_VOTI + voto.getId() /*Da aggiungere l'id dell'esame voto.getId()*/);
                    startActivity(i);
                }else {
                    Toast.makeText(context,"Internet assente",Toast.LENGTH_LONG).show();
                }
            }
        };

        listVoti = (ListView)findViewById(R.id.listVoti);
        listVoti.setAdapter(dataPoolVoti.getAdapter());

        final Esse3Connection connection =  Esse3Connection.getInstance();
        connection.getPage(Esse3Url.BACHECA_VOTI, new PageListener() {
            @Override
            public void onPageLoadDone(Document document) {
                dataPoolVoti.load(document);
                progressBar.setVisibility(View.GONE);
                if(!dataPoolVoti.isEmpty())textNoVoti.setText("");
                else textNoVoti.setText("Nessun voto pendente");
            }

            @Override
            public void onPageLoadFailed(Exception e) {

                if(isOnline()){
                    Toast.makeText(context, "Sessione scaduta", Toast.LENGTH_LONG).show();
                    connection.close();
                    Intent i = new Intent(context, LoginActivity.class);
                    startActivity(i);
                }else{
                    Toast.makeText(context,"Internet Assente",Toast.LENGTH_LONG).show();
                }
                progressBar.setVisibility(View.GONE);
            }
        });



    }

    public final boolean isOnline() {//funzione da spostare in connection
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                        return true;
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(android.view.Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_blanck, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }else if (item.getItemId() == android.R.id.home){
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}
