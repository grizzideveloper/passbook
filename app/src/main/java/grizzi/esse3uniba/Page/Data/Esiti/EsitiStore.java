package grizzi.esse3uniba.Page.Data.Esiti;

import android.content.Context;
import android.content.SharedPreferences;

import grizzi.esse3uniba.Page.Data.Element.ElementVoti;
import grizzi.esse3uniba.My.MyApplication;

/**
 * Created by Graziano on 29/03/2016.
 */
public class EsitiStore {

    SharedPreferences preferences = null;
    Context context;
    MyApplication app = MyApplication.getInstance();





    public EsitiStore(Context context){

        this.context = context;
        preferences = context.getSharedPreferences("ESITI",
                   Context.MODE_PRIVATE);



    }

    public void add(ElementVoti voto){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(voto.getId(),1);
        editor.apply();
        //solleva notifica
    }

    public boolean contains(ElementVoti voto){
        boolean result = false;
        if(preferences.getInt(voto.getId(),0) == 1) {
            result = true;
        }

        //alternativa
        /*
        result = preferences.contains(voto.getId());
        */

        return result;
    }

    public void clear(){
        preferences.edit().clear().apply();
        preferences.edit().apply();
    }

}
