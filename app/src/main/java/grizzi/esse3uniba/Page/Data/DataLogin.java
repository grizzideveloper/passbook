package grizzi.esse3uniba.Page.Data;


import android.content.Intent;
import android.util.Log;

import org.jsoup.nodes.Document;


/**
 * Created by Graziano on 13/04/2016.
 */
public class DataLogin extends Data {
    private boolean multiCarrer = false;


    public void load(Document document){
        if (document != null && document.toString().toLowerCase().contains("scegli carriera")) {
            multiCarrer = true;
        }
    }

    public void updateView(){}

    public boolean isMultiCarrer() {
        return multiCarrer;
    }

}
