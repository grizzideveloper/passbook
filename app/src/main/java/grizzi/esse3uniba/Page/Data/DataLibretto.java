package grizzi.esse3uniba.Page.Data;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.nfc.FormatException;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.hookedonplay.decoviewlib.DecoView;
import com.hookedonplay.decoviewlib.charts.SeriesItem;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;

import grizzi.esse3uniba.Page.DataMedie;
import grizzi.esse3uniba.R;
import grizzi.esse3uniba.Page.Data.Element.ElementLibretto;

/**
 * Created by Graziano on 05/02/2016.
 */
public class DataLibretto extends Data {

    ArrayList<ElementLibretto> listaVoci = new ArrayList<>();
    LibrettoAdapter adapter;

    private DataMedie medie = new DataMedie("0.0","0.0");

    private double mediaPonderata;
    private double mediaAritmentica;

    private String mPonderata  = "";
    private String mAritmetica = "";

    private HashMap<String,LinkedList<ElementLibretto>> mappaVoci = new HashMap<>();
    private LinkedList<String> listaNomi = new LinkedList<>();

    public DataLibretto(Context context){
        adapter = new LibrettoAdapter(context, R.layout.libretto_row,listaVoci);
    }

    public void load(Document document){

        Elements detailsTables = document.getElementsByClass("detail_table");
        if (detailsTables.size() > 0) {
            Elements rows = detailsTables.get(0).getElementsByTag("tr");
            for (int i = 0; i < rows.size(); i++) {
                Element row = rows.get(i);

                try{
                    ElementLibretto elementLibretto = new ElementLibretto(row);
                    listaVoci.add(elementLibretto);
                }catch (FormatException e){

                }
            }

        }

        Elements elements = document.getElementsByClass("tplMaster");
        if(elements.size() > 0) {
            String stringaAritmetica = elements.get(1).ownText();
            String stringaPonderata = elements.get(3).ownText();
            mAritmetica = stringaAritmetica.substring(0,stringaAritmetica.indexOf("/")).replaceAll("/","");
            mPonderata = stringaPonderata.substring(0,stringaAritmetica.indexOf("/")).replaceAll("/","");
            try{
                double nMArtimetica = Double.parseDouble(mAritmetica);
                setMediaAritmentica(nMArtimetica);
            }catch (NumberFormatException e){
                setMediaAritmentica(0);
            }
            try{
                double nMPonderata = Double.parseDouble(mPonderata);
                setMediaPonderata(nMPonderata);
            }catch (NumberFormatException e){
                setMediaPonderata(0);
            }
        }else{
            setMediaPonderata(0);
            setMediaAritmentica(0);
        }

        medie.setAritmetica("" + getMediaAritmentica());
        medie.setPonderata("" + getMediaPonderata());


        setCacheValid(true);
        updateView();
    }

    @Override
    public void updateView(){
        adapter.notifyDataSetChanged();
    }

    public double getMediaPonderata() {
        return mediaPonderata;
    }

    public void setMediaPonderata(double mediaPonderata) {
        this.mediaPonderata = mediaPonderata;
    }

    public double getMediaAritmentica() {
        return mediaAritmentica;
    }

    public void setMediaAritmentica(double mediaAritmentica) {
        this.mediaAritmentica = mediaAritmentica;
    }

    public ArrayList<ElementLibretto> getListaVoci() {
        return listaVoci;
    }

    public DataMedie getMedie() {
        return medie;
    }

    public String getmPonderata() {
        return mPonderata;
    }

    public void setmPonderata(String mPonderata) {
        this.mPonderata = mPonderata;
    }

    public String getmAritmetica() {
        return mAritmetica;
    }

    public void setmAritmetica(String mAritmetica) {
        this.mAritmetica = mAritmetica;
    }

    public ElementLibretto get(int position){
        return listaVoci.get(position);
    }

    public LibrettoAdapter getAdapter(){return adapter;}

    public class LibrettoAdapter extends ArrayAdapter<ElementLibretto> {

        public LibrettoAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        public LibrettoAdapter(Context context, int resource, List<ElementLibretto> items) {
            super(context, resource, items);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View v = convertView;


            LayoutInflater vi;
            vi = LayoutInflater.from(getContext());
            v = vi.inflate(R.layout.libretto_row_new_new, null);

            if(position%2==1)v.setBackgroundColor(Color.parseColor("#dddddd"));

            TextView matricola = (TextView)v.findViewById(R.id.textCFU);
            TextView corso = (TextView)v.findViewById(R.id.textNomeCorso);
            TextView stato = (TextView)v.findViewById(R.id.textStato);
            TextView data = (TextView)v.findViewById(R.id.textData);

            View votol = (View) v.findViewById(R.id.layout_voto);

            corso.setText(getItem(position).getNomeCorso());
            //corso.setTypeface(null, Typeface.BOLD);

            int voto =0;


/////////////////////////////////////////////////////////////////////////////////////////////////////

            try{
                if(getItem(position).getVotoEDataEsameSvolto().isEmpty()){
                    stato.setText("CFU: " + getItem(position).getCFU());
                    matricola.setText("N/D");
                    votol.setVisibility(View.GONE);
                }else if(!getItem(position).getVotoEDataEsameSvolto().substring(0,1).equals("I") && getItem(position).getVotoEDataEsameSvolto().length() > 5){
                    stato.setText("CFU: " + getItem(position).getCFU());
                    voto = Integer.parseInt(getItem(position).getVotoEDataEsameSvolto().substring(0,2));
                    matricola.setText(getItem(position).getVotoEDataEsameSvolto().substring(0,getItem(position).getVotoEDataEsameSvolto().indexOf("-")-1));//??
                    data.setText(getItem(position).getVotoEDataEsameSvolto().substring(getItem(position).getVotoEDataEsameSvolto().indexOf("-")+2));
                }else if(getItem(position).getVotoEDataEsameSvolto().length() > 5) {
                    stato.setText("CFU: " + getItem(position).getCFU());
                    matricola.setText("I");
                    voto = 30;
                }
            }catch (NumberFormatException e){
                stato.setText("CFU: " + getItem(position).getCFU());
                voto = 18;
                matricola.setText(getItem(position).getVotoEDataEsameSvolto().substring(0,getItem(position).getVotoEDataEsameSvolto().indexOf("-")-1));
            }

/////////////////////////////////////////////////////////////////////////////////////////////


/*
            DecoView decoView = (DecoView) v.findViewById(R.id.dynamicArcView);

            SeriesItem seriesItem = new SeriesItem.Builder(Color.parseColor("#FFE2E2E2"))
                    .setRange(0, 30, 30)
                    .build();
            decoView.addSeries(seriesItem);

            if(voto > 0) {
                SeriesItem seriesVoto = new SeriesItem.Builder(Color.parseColor("#3F51B5"))
                    .setRange(0, 30,voto)
                    .build();
                decoView.addSeries(seriesVoto);
            }*/




            return v;
        }

    }


    public class ExpandableListAdapter extends BaseExpandableListAdapter {

        private Context _context;
        private List<String> _listDataHeader; // header titles
        // child data in format of header title, child title
        private HashMap<String, LinkedList<ElementLibretto>> _listDataChild;


        public ExpandableListAdapter(Context context, List<String> listDataHeader,
                                     HashMap<String, LinkedList<ElementLibretto>> listChildData) {
            this._context = context;
            this._listDataHeader = listDataHeader;
            this._listDataChild = listChildData;
        }

        @Override
        public Object getChild(int groupPosition, int childPosititon) {
            return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .get(childPosititon);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getChildView(int groupPosition, final int childPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {

            final ElementLibretto child = (ElementLibretto) getChild(groupPosition, childPosition);




            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this._context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.list_item_libretto, null);
            }

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //onItemClick(child);
                }
            });

            TextView txtListChild = (TextView) convertView
                    .findViewById(R.id.textCFU);

            TextView data = (TextView) convertView
                    .findViewById(R.id.textData);

            TextView voto = (TextView) convertView
                    .findViewById(R.id.textVoto);



            if(child.getVotoEDataEsameSvolto().isEmpty()){
                txtListChild.setText("Esame non sostenuto");
                data.setText("CFU: " + child.getCFU());
                voto.setText("Esito: N/D");
            }
            else{
                txtListChild.setText("Esame sostenuto in data: " + child.getVotoEDataEsameSvolto().substring(5));
                data.setText("CFU: " + child.getCFU());
                voto.setText("Esito: " +  child.getVotoEDataEsameSvolto().substring(0,2));
            }
            return convertView;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .size();
        }

        @Override
        public Object getGroup(int groupPosition) {

            return this._listDataHeader.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return _listDataHeader.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            /**/
            return groupPosition;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded,
                                 View convertView, ViewGroup parent) {
            String headerTitle = (String) getGroup(groupPosition);
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this._context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.list_group, null);
            }

            TextView lblListHeader = (TextView) convertView
                    .findViewById(R.id.lblListHeader);
            lblListHeader.setTypeface(null, Typeface.BOLD);
            lblListHeader.setText(headerTitle);

            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

        public void notifyDataSetInvalidated()
        {
            super.notifyDataSetInvalidated();
        }



    }
}
