package grizzi.esse3uniba.Page.Data;

import android.content.Context;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import grizzi.esse3uniba.Page.Data.Data;

/**
 * Created by Graziano on 13/02/2016.
 */
public class DataDettagliAppello extends Data{
    private String attivita;
    private String appello;
    private String sessioni;
    private String tipoEsame;
    private String docenti;

    public DataDettagliAppello(Context context){

    }

    public void load(Document document){
        Elements tables = document.select("table");

        Element element = document.getElementById("app-box_dati_pren"); //tables.get(3);

        Elements detailsTables = element.getElementsByTag("description");


        if(detailsTables.size() > 0)setAttivita(detailsTables.get(0).ownText());
        else setAttivita("N/D");
        if(detailsTables.size() > 1)setAppello(detailsTables.get(1).ownText());
        else setAppello("N/D");
        if(detailsTables.size() > 2)setSessioni(detailsTables.get(2).ownText());
        else setSessioni("N/D");
        if(detailsTables.size() > 3)setTipoEsame(detailsTables.get(3).ownText());
        else setTipoEsame("N/D");
        if(detailsTables.size() > 4)setDocenti(detailsTables.get(4).ownText());
        else setDocenti("N/D");
    }

    @Override
    public void updateView(){
    }

    public String getAttivita() {
        return attivita;
    }

    public void setAttivita(String attivita) {
        this.attivita = attivita;
    }

    public String getAppello() {
        return appello;
    }

    public void setAppello(String appello) {
        this.appello = appello;
    }

    public String getSessioni() {
        return sessioni;
    }

    public void setSessioni(String sessioni) {
        this.sessioni = sessioni;
    }

    public String getTipoEsame() {
        return tipoEsame;
    }

    public void setTipoEsame(String tipoEsame) {
        this.tipoEsame = tipoEsame;
    }

    public String getDocenti() {
        return docenti;
    }

    public void setDocenti(String docenti) {
        this.docenti = docenti;
    }
}
