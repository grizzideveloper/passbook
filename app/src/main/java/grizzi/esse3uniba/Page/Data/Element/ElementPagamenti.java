package grizzi.esse3uniba.Page.Data.Element;

import org.jsoup.select.Elements;

/**
 * Created by Graziano on 05/02/2016.
 */
public class ElementPagamenti extends Element {

    String  fatturaId = "N/D",
            codiceBollettino = "N/D",
            anno = "N/D",
            descrizione = "N/D",
            dataScadenza = "N/D",
            importo = "N/D";

    int stato = 0;


    public ElementPagamenti(org.jsoup.nodes.Element row)throws VoidElementException{

        Elements cells = row.getElementsByTag("td");

        try {
            if(cells.size()>1){
                setFatturaId(cells.get(0).child(0).ownText());
                setCodiceBollettino(cells.get(2).ownText());
                setAnno(cells.get(3).ownText());
                setDescrizione(cells.get(4).ownText());
                setDataScadenza(cells.get(3).ownText());
                setImporto(cells.get(4).ownText());

                if(cells.get(5).children().size() > 0){
                    if(cells.get(5).child(0).attr("src").contains("semaf_r.gif")){
                        setStato(0);
                    }else setStato(1);
                }else{
                    setStato(2);
                }
            }
        }catch (Exception e){
            setFatturaId("ERRORE CARICAMENTO DATI");
        }
    }

    public int getStato() {
        return stato;
    }

    public void setStato(int stato) {
        this.stato = stato;
    }

    public String getFatturaId() {
        return fatturaId;
    }

    public void setFatturaId(String fatturaId) {
        this.fatturaId = fatturaId;
    }

    public String getCodiceBollettino() {
        return codiceBollettino;
    }

    public void setCodiceBollettino(String codiceBollettino) {
        this.codiceBollettino = codiceBollettino;
    }

    public String getAnno() {
        return anno;
    }

    public void setAnno(String anno) {
        this.anno = anno;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public String getDataScadenza() {
        return dataScadenza;
    }

    public void setDataScadenza(String dataScadenza) {
        this.dataScadenza = dataScadenza;
    }

    public String getImporto() {
        return importo;
    }

    public void setImporto(String importo) {
        this.importo = importo;
    }
}
