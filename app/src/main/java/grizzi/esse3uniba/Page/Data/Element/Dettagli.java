package grizzi.esse3uniba.Page.Data.Element;

/**
 * Created by Graziano on 01/04/2016.
 */
 class Dettagli{

    private String name;
    private String value;

    Dettagli(String name,String value){
        setName(name);
        setValue(value);
    }

    private String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }

    private String getValue() {
        return value;
    }

    private void setValue(String value) {
        this.value = value;
    }

    public String getDettaglio(){
        return name + "=" + value + "&";
    }
}