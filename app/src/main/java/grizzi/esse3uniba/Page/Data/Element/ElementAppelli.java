package grizzi.esse3uniba.Page.Data.Element;

import org.jsoup.select.Elements;

import java.util.LinkedList;

/**
 * Created by Graziano on 05/02/2016.
 */
public class ElementAppelli extends Element {

    private String nomeAppello = "prova";
    private String data= "prova";
    private String descrizione= "prova";
    private String subscribedNum= "prova";

    private LinkedList<Dettagli> dettagliAppello;

    private Boolean prenotabile = false;



    public ElementAppelli(org.jsoup.nodes.Element row)throws VoidElementException{

        Elements cells = row.getElementsByTag("td");

        setNomeAppello(cells.get(1).text().trim());
        setData(cells.get(2).text().trim());
        setSubscribedNum(cells.get(3).text().trim());
        setDescrizione(cells.get(4).text().trim());


        dettagliAppello = new LinkedList<>();

        //Elements elements = cells.get(0).getElementsByTag("input");
        Elements elements = cells.get(0).getElementsByTag("a");

        if(elements.size() > 0){//set prenotabile
            setPrenotabile(true);

            for(int i = 0;i<elements.size();i++){
                String url = elements.get(i).attr("href");
                dettagliAppello.add(new Dettagli(elements.get(i).attr("name"),url.substring(url.indexOf("?")+1)));
            }
        }
    }


    public String getSubscribedNum() {
        return subscribedNum;
    }

    public void setSubscribedNum(String subscribedNum) {
        this.subscribedNum = subscribedNum;
    }

    public String getNomeAppello() {
        return nomeAppello;
    }

    public void setNomeAppello(String nomeAppello) {
        this.nomeAppello = nomeAppello;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }


    private Boolean getPrenotabile() {
        return prenotabile;
    }

    private void setPrenotabile(Boolean prenotabile) {
        this.prenotabile = prenotabile;
    }

    public Boolean prenotabile(){
        return prenotabile;
    }


    public String getId(){
        String id = "?";
        for(int i = 0;i < dettagliAppello.size();i++){
            id = id + dettagliAppello.get(i).getDettaglio();
        }

        return id;
    }



    private class Dettagli {

        private String name;
        private String value;

        Dettagli(String name, String value){
            setName(name);
            setValue(value);
        }

        private String getName() {
            return name;
        }

        private void setName(String name) {
            this.name = name;
        }

        private String getValue() {
            return value;
        }

        private void setValue(String value) {
            this.value = value;
        }

        public String getDettaglio(){
            return name + "=" + value + "&";
        }
    }



}
