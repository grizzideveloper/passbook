package grizzi.esse3uniba.Page.Data;

import android.content.Context;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import grizzi.esse3uniba.Page.Data.Data;
import grizzi.esse3uniba.Utils.CrashReport;

/**
 * Created by Graziano on 01/04/2016.
 */
public class DataMenu extends Data {

    private String matricola = "";
    private String nome = "";

    public DataMenu(Context context){

    }
    
    public void load(Document document){
        try{
            Element elementTitle  = document.getElementById("page-title");
            String element = elementTitle.text();
            try{
                setMatricola(element.substring(element.indexOf(".") + 1,element.indexOf("]")));
            }catch (Exception e){
                setMatricola(element);
            }
            try{
                setNome(element.substring(0,element.indexOf("-")));
            }catch (Exception e){
                setNome(element);
            }
        }catch (Exception e){
            CrashReport.reportException(e,this.getClass().getName());
        }
    }

    public String getMatricola() {
        return matricola;
    }

    private void setMatricola(String matricola) {
        this.matricola = matricola;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public void updateView(){}
}
