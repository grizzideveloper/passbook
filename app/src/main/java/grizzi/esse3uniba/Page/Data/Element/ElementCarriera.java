package grizzi.esse3uniba.Page.Data.Element;

import android.widget.Toast;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import grizzi.esse3uniba.Page.Data.Data;

/**
 * Created by Graziano on 18/01/2016.
 */
public class ElementCarriera extends Element {//Sarebbe meglio creare una exception

    private String matricola = "none";
    private String nomeCorso = "none";
    private String tipoCorso = "none";
    private String statoCorso = "none";
    private String urlCarriera = "none";


    public ElementCarriera(Elements rawCarriera)throws VoidElementException{
        if(rawCarriera != null){
            matricola = rawCarriera.get(0).text();
            tipoCorso = rawCarriera.get(1).text();
            nomeCorso = rawCarriera.get(2).text();
            statoCorso = rawCarriera.get(3).text();
            urlCarriera = rawCarriera.get(5).absUrl("href");
        }
        else {
            matricola = "none";
            tipoCorso = "none";
            nomeCorso = "none";
            statoCorso = "none";
            urlCarriera = "none";
        }
    }

    public String toString(){
        String stringToReturn = "Matricola: " + matricola + " Corso: " + tipoCorso + " " + nomeCorso + " Stato: " + statoCorso;
        return stringToReturn;
    }

    public String getMatricola() {
        return matricola;
    }

    public void setMatricola(String matricola) {
        this.matricola = matricola;
    }

    public String getNomeCorso() {
        return nomeCorso;
    }

    public void setNomeCorso(String nomeCorso) {
        this.nomeCorso = nomeCorso;
    }

    public String getTipoCorso() {
        return tipoCorso;
    }

    public void setTipoCorso(String tipoCorso) {
        this.tipoCorso = tipoCorso;
    }

    public String getStatoCorso() {
        return statoCorso;
    }

    public void setStatoCorso(String statoCorso) {
        this.statoCorso = statoCorso;
    }

    public String getUrlCarriera() {
        return urlCarriera;
    }

    public void setUrlCarriera(String urlCarriera) {
        this.urlCarriera = urlCarriera;
    }
}
