package grizzi.esse3uniba.Page.Data;

import android.content.Context;
import android.widget.ExpandableListAdapter;

import org.jsoup.nodes.Document;

import java.util.LinkedList;

import grizzi.esse3uniba.Page.Data.Element.Element;

/**
 * Created by Graziano on 03/03/2016.
 */
public abstract class Data {

    private LinkedList<Element> listaVoci = new LinkedList<>();
    private Boolean cacheValid =  false;
    private String filter = new String();

    public abstract void load(Document document);
    public abstract void updateView();

    public Element get(int position){
        return listaVoci.get(position);
    }

    public void erease(){
        while (!listaVoci.isEmpty()) {
            listaVoci.removeFirst();
        }
    }

    public Boolean getCacheValid() {
        return cacheValid;
    }

    protected void setCacheValid(Boolean cacheValid) {
        this.cacheValid = cacheValid;
    }

    public void invalidateCache(){
        setCacheValid(false);
    }

    protected String getFilter(){
        return filter;
    }

    public void setFilter(String filter){
        this.filter = filter;
    }



}
