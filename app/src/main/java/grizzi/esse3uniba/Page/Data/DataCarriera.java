package grizzi.esse3uniba.Page.Data;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import grizzi.esse3uniba.Esse3.Esse3Connection;
import grizzi.esse3uniba.Esse3.UserStore;
import grizzi.esse3uniba.My.MyApplication;
import grizzi.esse3uniba.Page.Data.Element.ElementCarriera;
import grizzi.esse3uniba.Page.Data.Element.VoidElementException;
import grizzi.esse3uniba.Page.PageListener;
import grizzi.esse3uniba.R;

/**
 * Created by Graziano on 02/04/2016.
 */
public class DataCarriera extends Data {

    LinkedList<ElementCarriera> listaElement = new LinkedList<>();
    CareerAdapter adapter;
    Context context;
    UserStore userStore;

    public DataCarriera(Context context){
        adapter = new CareerAdapter(context,R.layout.career_row,listaElement);
        this.context = context;
        userStore = UserStore.getInstance(context);
    }

    public void load(Document document){
        Elements detailsTables = document.getElementsByClass("table-1-body");//ogni elemento è una tabella
        Elements Row = detailsTables.get(0).getElementsByTag("tr");//lo 0 è l'indice della tabella che ci interessa

        for (int i = 0; i < Row.size(); i++) {
            Element firstRow = Row.get(i);//estraggo il record i della tabella
            Elements td = firstRow.getElementsByTag("td");
            Elements anchors = firstRow.getElementsByTag("a");//estraggo valori dei campi dall record//ogni elemnto è un campo e  vengono conservati anche gli url
            td.addAll(anchors);
            try{
                listaElement.add(new ElementCarriera(td));//passo i campi alla funzione che ne estrarrà i valori e ne crea una nuova carriera da aggiungere alla lista
            }catch (VoidElementException e){
                continue;
            }

        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void updateView(){
        adapter.notifyDataSetChanged();
    }

    public CareerAdapter getAdapter() {
        return adapter;
    }

    private void setAdapter(CareerAdapter adapter) {
        this.adapter = adapter;
    }

    private class CareerAdapter extends ArrayAdapter<ElementCarriera> {

        public CareerAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        public CareerAdapter(Context context, int resource, List<ElementCarriera> items) {
            super(context, resource, items);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View v = convertView;

            if (v == null) {
                LayoutInflater vi;
                vi = LayoutInflater.from(getContext());
                v = vi.inflate(R.layout.career_row, null);
            }

            TextView matricola = (TextView)v.findViewById(R.id.textMatricola);
            TextView corso = (TextView)v.findViewById(R.id.textCorso);
            TextView stato = (TextView)v.findViewById(R.id.textStato);

            matricola.setText("Matricola: " + getItem(position).getMatricola());
            corso.setText(getItem(position).getNomeCorso());
            stato.setText("Stato: " + getItem(position).getStatoCorso());

            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                }
            });

            v.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View view, MotionEvent event) {
                    if (event.getAction() == android.view.MotionEvent.ACTION_DOWN) {
                        final ProgressDialog ringProgressDialog = ProgressDialog.show(view.getContext(), "Attendere ...", "Apertura carriera ...", true);
                        view.setBackgroundColor(view.getResources().getColor(R.color.sfondo));
                        view.invalidate();
                        final Context context = view.getContext();
                        MyApplication app = MyApplication.getInstance();
                        final Esse3Connection connection = Esse3Connection.getInstance();
                        String urlC = getItem(position).getUrlCarriera();
                        connection.setId(urlC.substring(urlC.lastIndexOf("?") + 1));


                        userStore.saveUser(connection.getUser());

                        connection.getPage(getItem(position).getUrlCarriera(), new PageListener() {
                            @Override
                            public void onPageLoadDone(Document document) {
                                //ringProgressDialog.dismiss();
                                onClick();
                            }

                            @Override
                            public void onPageLoadFailed(Exception e) {
                                ringProgressDialog.dismiss();
                            }

                        });
                    } else if (event.getAction() == android.view.MotionEvent.ACTION_UP) {
                        view.setBackgroundColor(view.getResources().getColor(R.color.tabsScrollColor));
                        view.invalidate();
                    }
                    return true;
                }
            });


            return v;
        }

    }

    public void onClick(){

    }


}
