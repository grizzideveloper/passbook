package grizzi.esse3uniba.Page.Data;

import android.app.AlertDialog;
import android.app.DownloadManager;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Typeface;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;



import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import grizzi.esse3uniba.Page.Data.Element.VoidElementException;
import grizzi.esse3uniba.R;
import grizzi.esse3uniba.Page.Data.Element.ElementLibretto;
import grizzi.esse3uniba.Page.Data.Element.ElementPagamenti;

/**
 * Created by Graziano on 05/02/2016.
 */
public class DataPagamenti extends Data {

    LinkedList<ElementPagamenti> listaVoci = new LinkedList<>();
    PagamentiAdapter adapter;

    private DownloadManager downloadManager;


    private HashMap<String,LinkedList<ElementLibretto>> mappaVoci = new HashMap<>();
    private LinkedList<String> listaNomi = new LinkedList<>();

    public DataPagamenti(Context context){

        adapter = new PagamentiAdapter(context, R.layout.libretto_row,listaVoci);
        //adapter = new ExpandableListAdapter(context,listaNomi,mappaVoci);
        downloadManager = (DownloadManager)context.getSystemService(context.DOWNLOAD_SERVICE);
    }

    public void load(Document document){

        //Element div = document.getElementById("esse3old");
        Elements table = document.getElementsByClass("table-1-body");
        Elements tr = table.get(0).getElementsByTag("tr");
        for(int i = 0; i< tr.size();i++)
        {
            try{
                if(!tr.get(i).attr("class").contains("tplTitolo"))
                    listaVoci.add(new ElementPagamenti(tr.get(i)));
            }catch (VoidElementException e){
                continue;
            }

        }
        adapter.notifyDataSetChanged();
        setCacheValid(true);
    }

    @Override
    public void updateView(){
        adapter.notifyDataSetChanged();
    }

    public ElementPagamenti get(int position){
        return listaVoci.get(position);
    }

    public PagamentiAdapter getAdapter(){return adapter;}





    public class PagamentiAdapter extends ArrayAdapter<ElementPagamenti> {

        public PagamentiAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        public PagamentiAdapter(Context context, int resource, List<ElementPagamenti> items) {
            super(context, resource, items);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View v = convertView;

            if (v == null) {
                LayoutInflater vi;
                vi = LayoutInflater.from(getContext());
                v = vi.inflate(R.layout.pagamenti_row, null);
            }

            TextView codice = (TextView)v.findViewById(R.id.textData);
            TextView descrizione = (TextView)v.findViewById(R.id.textNome);
            TextView scadenza = (TextView)v.findViewById(R.id.info);
            TextView scarica = (TextView)v.findViewById(R.id.delete);
            codice.setText(getItem(position).getDescrizione() + "  " +getItem(position).getFatturaId() + " - " + getItem(position).getImporto());
            if(getItem(position).getStato() == 1){
                descrizione.setText("Pagato");
                descrizione.setTextColor(Color.GRAY);
            }else if(getItem(position).getStato() == 0){
                descrizione.setText("Non pagato");
                descrizione.setTextColor(Color.parseColor("#F44336"));
            }else {
                descrizione.setText("Non definito");
                descrizione.setTextColor(Color.parseColor("#F44336"));
            }
            scadenza.setText("Scadenza: " + getItem(position).getDataScadenza());
            scarica.setText("MAV");
            scarica.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                   /* Intent i = new Intent(getContext(), MyWebView.class);
                    i.putExtra("URL", "https://www.studenti.ict.uniba.it/esse3/auth/studente/Tasse/FatturaDettaglio.do?fatt_id=" + getItem(position).getFatturaId());
                    getContext().startActivity(i);*/
                    //Uri uri = Uri.parse("https://www.studenti.ict.uniba.it/esse3/auth/studente/Tasse/StampaMav.do;jsessionid=" + Esse3Connection.getInstance().getCookie() + "&" + Esse3Connection.getInstance().getId() + "&fatt_id=4073967&fatt_cod=00000000000004073967&btnSubmit=Stampa+MAV"/*"?fatt_id=" + getItem(position).getFatturaId() + "&" + Esse3Connection.getInstance().getId()*/);
                    /*DownloadManager.Request request = new DownloadManager.Request(uri);
                    downloadManager.enqueue(request);
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, uri);
                    getContext().startActivity(browserIntent);*/

                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                    builder1.setMessage("Mi dispiace non è ancora possibile scaricare il mav");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {

                                    dialog.cancel();
                                }
                            });


                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }
            });


            return v;
        }

    }


    public class ExpandableListAdapter extends BaseExpandableListAdapter {

        private Context _context;
        private List<String> _listDataHeader; // header titles
        // child data in format of header title, child title
        private HashMap<String, LinkedList<ElementLibretto>> _listDataChild;


        public ExpandableListAdapter(Context context, List<String> listDataHeader,
                                     HashMap<String, LinkedList<ElementLibretto>> listChildData) {
            this._context = context;
            this._listDataHeader = listDataHeader;
            this._listDataChild = listChildData;
        }

        @Override
        public Object getChild(int groupPosition, int childPosititon) {
            return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .get(childPosititon);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getChildView(int groupPosition, final int childPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {

            final ElementLibretto child = (ElementLibretto) getChild(groupPosition, childPosition);




            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this._context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.list_item_libretto, null);
            }

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //onItemClick(child);
                }
            });

            TextView txtListChild = (TextView) convertView
                    .findViewById(R.id.textCFU);

            TextView data = (TextView) convertView
                    .findViewById(R.id.textData);

            TextView voto = (TextView) convertView
                    .findViewById(R.id.textVoto);



            if(child.getVotoEDataEsameSvolto().isEmpty()){
                txtListChild.setText("Esame non sostenuto");
                data.setText("CFU: " + child.getCFU());
                voto.setText("Esito: N/D");
            }
            else{
                txtListChild.setText("Esame sostenuto in data: " + child.getVotoEDataEsameSvolto().substring(5));
                data.setText("CFU: " + child.getCFU());
                voto.setText("Esito: " +  child.getVotoEDataEsameSvolto().substring(0,2));
            }
            return convertView;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .size();
        }

        @Override
        public Object getGroup(int groupPosition) {

            return this._listDataHeader.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return _listDataHeader.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            /**/
            return groupPosition;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded,
                                 View convertView, ViewGroup parent) {
            String headerTitle = (String) getGroup(groupPosition);
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this._context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.list_group, null);
            }

            TextView lblListHeader = (TextView) convertView
                    .findViewById(R.id.lblListHeader);
            lblListHeader.setTypeface(null, Typeface.BOLD);
            lblListHeader.setText(headerTitle);

            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

        public void notifyDataSetInvalidated()
        {
            super.notifyDataSetInvalidated();
        }



    }
}
