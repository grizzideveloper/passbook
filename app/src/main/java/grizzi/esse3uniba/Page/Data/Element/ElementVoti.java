package grizzi.esse3uniba.Page.Data.Element;

import org.jsoup.select.Elements;

import java.util.LinkedList;

/**
 * Created by Graziano on 13/02/2016.
 */
public class ElementVoti extends Element {

    private String dataEsame = "null";
    private String ora = "null";
    private String docenti = "null";
    private String nomeEsame = "null";
    private String dataChiusura = "null";
    private String voto = "null";
    private int stato = 0;

    LinkedList<Dettagli> dettagliVoto = new LinkedList<>();


    public ElementVoti(Elements elements, Elements tableStruct)throws VoidElementException{

        try{
            if(elements.size() > 1){

                setStato(0);

                setDataEsame(elements.get(0).ownText());
                setOra(elements.get(1).ownText());
                setDocenti(elements.get(2).ownText());
                setDataChiusura(elements.get(3).ownText());
                setNomeEsame(tableStruct.get(0).ownText());
                setVoto(elements.get(4).child(0).ownText());

                Elements input = elements.get(4).getElementsByTag("input");

                for(int i = 0;i< input.size() - 1;i++){
                    dettagliVoto.add(new Dettagli(input.get(i).attr("name"), input.get(i).attr("value")));
                }


                String statoVoto = input.get(input.size() - 1).attr("src");
                if(statoVoto.contains("4") || statoVoto.contains("3")){
                    setStato(1);
                }else if (statoVoto.contains("5") || statoVoto.contains("6")){
                    setStato(2);
                }


            }
        }catch(IndexOutOfBoundsException e){

        }

    }

    public int getStato() {
        return stato;
    }

    public void setStato(int stato) {
        this.stato = stato;
    }

    public String getDataEsame() {
        return dataEsame;
    }

    public void setDataEsame(String dataEsame) {
        this.dataEsame = dataEsame;
    }

    public String getOra() {
        return ora;
    }

    public void setOra(String ora) {
        this.ora = ora;
    }

    public String getDocenti() {
        return docenti;
    }

    public void setDocenti(String docenti) {
        this.docenti = docenti;
    }

    public String getNomeEsame() {
        return nomeEsame;
    }

    public void setNomeEsame(String nomeEsame) {
        this.nomeEsame = nomeEsame;
    }

    public String getDataChiusura() {
        return dataChiusura;
    }

    public void setDataChiusura(String dataChiusura) {
        this.dataChiusura = dataChiusura;
    }

    public String getVoto() {
        return voto;
    }

    public void setVoto(String voto) {
        this.voto = voto;
    }

    public String getId(){
        String id = "?";
        for(int i = 0;i < dettagliVoto.size();i++){
            id = id + dettagliVoto.get(i).getDettaglio();
        }
        return id;
    }
}
