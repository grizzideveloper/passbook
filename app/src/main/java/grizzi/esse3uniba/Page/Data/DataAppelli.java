package grizzi.esse3uniba.Page.Data;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


import android.widget.ImageView;
import android.widget.TextView;


import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.LinkedList;
import java.util.List;

import java.util.HashMap;


import android.graphics.Typeface;
import android.widget.BaseExpandableListAdapter;

import grizzi.esse3uniba.Page.Data.Element.VoidElementException;
import grizzi.esse3uniba.R;
import grizzi.esse3uniba.Page.Data.Element.ElementAppelli;


/**
 * Created by Graziano on 05/02/2016.
 */
public class DataAppelli extends Data {

    //LinkedList<VoceAppelli> listaVoci = new LinkedList<>();
    private Context context;
    private ExpandableListAdapter expandableAdapter;

    private HashMap<String,LinkedList<ElementAppelli>> mappaVoci;
    private LinkedList<String> listaNomiAppelli;

    private String filter = new String();



    public DataAppelli(Context context){
        this.context = context;
        mappaVoci = new HashMap<>();
        listaNomiAppelli = new LinkedList<>();
        expandableAdapter = new ExpandableListAdapter(context,listaNomiAppelli,mappaVoci);
    }

    @Override
    public void load(Document document){
        Elements detailsTables = document.getElementsByClass("table-1-body");
        LinkedList<ElementAppelli> listaDateEsame = new LinkedList<>();

        if (detailsTables.size() > 0) {
            Elements rows = detailsTables.get(0).getElementsByTag("tr");
            for (int i = 1; i < rows.size(); i++) {
                try{
                    Element row = rows.get(i);
                    ElementAppelli appello = new ElementAppelli(row);

                    if(listaDateEsame.isEmpty()){
                        //se la lista delle date è vuota o se l'appello attuale non è stato già inserito
                        listaNomiAppelli.add("");
                        mappaVoci.put(listaNomiAppelli.getLast(), listaDateEsame);
                        listaDateEsame = new LinkedList<>();
                        listaDateEsame.add(appello);
                        listaNomiAppelli.add(appello.getNomeAppello());

                    }else if(!listaNomiAppelli.getLast().equals(appello.getNomeAppello()) ){
                        mappaVoci.put(listaNomiAppelli.getLast(), listaDateEsame);
                        listaDateEsame = new LinkedList<>();
                        listaDateEsame.add(appello);
                        listaNomiAppelli.add(appello.getNomeAppello());

                    }else {
                        listaDateEsame.add(appello);
                    }
                }catch (VoidElementException e){

                    continue;
                }

            }
            if(!listaDateEsame.isEmpty()) mappaVoci.put(listaNomiAppelli.getLast(), listaDateEsame);
            else  System.out.println("ERRORE lista appelli vuota");
            expandableAdapter.notifyDataSetChanged();
        }

        setCacheValid(true);
    }


    @Override
    public void setFilter(String filter) {
        super.setFilter(filter);
        expandableAdapter.notifyDataSetChanged();
    }

    @Override
    public void updateView(){
        expandableAdapter.notifyDataSetChanged();
    }

    public ExpandableListAdapter getExpandableAdapter(){
        return expandableAdapter;
    }

    public void onItemClick(final ElementAppelli voce){

    }

    public class ExpandableListAdapter extends BaseExpandableListAdapter {

        private Context _context;
        private List<String> _listDataHeader; // header titles
        // child data in format of header title, child title
        private HashMap<String, LinkedList<ElementAppelli>> _listDataChild;


        public ExpandableListAdapter(Context context, List<String> listDataHeader,
                                     HashMap<String, LinkedList<ElementAppelli>> listChildData) {
            this._context = context;
            this._listDataHeader = listDataHeader;
            this._listDataChild = listChildData;
        }

        @Override
        public Object getChild(int groupPosition, int childPosititon) {
            int j = 0;

            if(groupPosition == 0){
                j = 1;
            }
            else {

                int finded = 0;

                for(j = 1;j<_listDataHeader.size()&& finded < groupPosition;j++){

                    String name = _listDataHeader.get(j);

                    if(name.toLowerCase().contains(getFilter().toLowerCase())){
                        finded++;
                    }
                }
            }
            return this._listDataChild.get(this._listDataHeader.get(j-1))
                    .get(childPosititon);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getChildView(int groupPosition, final int childPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {

            final ElementAppelli child = (ElementAppelli) getChild(groupPosition, childPosition);




            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this._context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.list_item, null);
            }

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClick(child);
                }
            });

            TextView txtListChild = (TextView) convertView
                    .findViewById(R.id.lblListItem);

            ImageView image = (ImageView)convertView.findViewById(R.id.imageView3);



            txtListChild.setText(child.getData());
            if(!child.prenotabile()){
                image.setImageResource(R.drawable.ic_close_black_48dp);
                txtListChild.setTextColor(Color.GRAY);
            }
            else{
                image.setImageResource(R.drawable.next_128);
                txtListChild.setTextColor(Color.BLACK);
            }
            return convertView;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .size();
        }

        @Override
        public Object getGroup(int groupPosition) {

            int j = 0;

            if(groupPosition == 0){
                j = 1;
            }
            else {

                int finded = 0;

                for(j = 1;j<_listDataHeader.size()&& finded < groupPosition;j++){

                    String name = _listDataHeader.get(j);

                    if(name.toLowerCase().contains(getFilter().toLowerCase())){
                        finded++;
                    }
                }
            }
            return this._listDataHeader.get(j-1);
        }

        @Override
        public int getGroupCount() {
            int filterdGroup = 0;
            for(int i = 0;i < _listDataHeader.size();i++){
                if(_listDataHeader.get(i).toLowerCase().contains(getFilter().toLowerCase())){
                    filterdGroup++;
                }
            }
            if(filterdGroup < _listDataHeader.size())filterdGroup++;
            return filterdGroup ;
        }

        @Override
        public long getGroupId(int groupPosition) {

            /**/
            return groupPosition;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded,
                                 View convertView, ViewGroup parent) {
            String headerTitle = (String) getGroup(groupPosition);
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this._context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.list_group, null);
            }

            TextView lblListHeader = (TextView) convertView
                    .findViewById(R.id.lblListHeader);
            lblListHeader.setTypeface(null, Typeface.BOLD);
            lblListHeader.setText(headerTitle);

            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

        public void notifyDataSetInvalidated()
        {
            super.notifyDataSetInvalidated();
        }



    }


}
