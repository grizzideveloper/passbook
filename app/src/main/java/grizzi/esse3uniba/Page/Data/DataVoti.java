package grizzi.esse3uniba.Page.Data;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.hookedonplay.decoviewlib.DecoView;
import com.hookedonplay.decoviewlib.charts.SeriesItem;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.LinkedList;
import java.util.List;

import grizzi.esse3uniba.Page.Data.Element.VoidElementException;
import grizzi.esse3uniba.Page.Data.Esiti.EsitiStore;
import grizzi.esse3uniba.R;
import grizzi.esse3uniba.Page.Data.Element.ElementVoti;

/**
 * Created by Graziano on 05/02/2016.
 */
public class DataVoti extends Data {

    LinkedList<ElementVoti> listaVoci = new LinkedList<>();
    LibrettoAdapter adapter;
    EsitiStore store;

    public DataVoti(Context context){

        adapter = new LibrettoAdapter(context, R.layout.libretto_row,listaVoci);
        store = new EsitiStore(context);
    }

    public void load(Document document){

        Elements tables = document.select("table");

        for(int i = 3;i< tables.size();i++){
            Elements detailsTables = tables.get(i).getElementsByTag("td");
            Elements nameTables = tables.get(i).getElementsByTag("th");
            try{
                ElementVoti voto = new ElementVoti(detailsTables, nameTables);
                listaVoci.add(voto);
                if(!store.contains(voto)){
                    store.add(voto);
                }
            }catch (VoidElementException e){
                continue;
            }
        }
        adapter.notifyDataSetChanged();

        setCacheValid(true);
    }

    @Override
    public void updateView(){
        adapter.notifyDataSetChanged();
    }

    public ElementVoti get(int position){
        return listaVoci.get(position);
    }

    public LibrettoAdapter getAdapter(){
        return adapter;
    }

    public Boolean isEmpty(){
        return listaVoci.isEmpty();
    }

    public void onItemClick(ElementVoti voto){
        
    }

    public class LibrettoAdapter extends ArrayAdapter<ElementVoti> {

        public LibrettoAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        public LibrettoAdapter(Context context, int resource, List<ElementVoti> items) {
            super(context, resource, items);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View v = convertView;

            if (v == null) {
                LayoutInflater vi;
                vi = LayoutInflater.from(getContext());
                v = vi.inflate(R.layout.voti_row, null);
            }


            TextView matricola = (TextView)v.findViewById(R.id.textCFU);
            TextView corso = (TextView)v.findViewById(R.id.textNomeCorso);
            TextView stato = (TextView)v.findViewById(R.id.textStato);
            TextView accetta = (TextView)v.findViewById(R.id.button);


            DecoView decoView = (DecoView) v.findViewById(R.id.dynamicArcView);

            SeriesItem seriesItem = new SeriesItem.Builder(Color.parseColor("#FFE2E2E2"))
                    .setRange(0, 30, 30)
                    .build();



            decoView.addSeries(seriesItem);

            int color = 0,baseColor = Color.parseColor("#3F51B5");

            switch (getItem(position).getStato()){
                case 0:
                    color = Color.parseColor("#FF9800");
                    accetta.setTextColor(Color.DKGRAY);
                    break;
                case 1:
                    color = Color.parseColor("#4CAF50");
                    break;
                case 2:
                    color = Color.parseColor("#F44336");
                    break;
            }

            if(getItem(position).getVoto().matches("\\d+(?:\\.\\d+)?"))
            {
                SeriesItem seriesVoto = new SeriesItem.Builder(baseColor)
                        .setRange(0, 30,Integer.parseInt(getItem(position).getVoto()))
                        .build();

                SeriesItem seriesStato = new SeriesItem.Builder(color)
                        .setRange(0, 32,8)
                        .build();


                decoView.addSeries(seriesVoto);
                decoView.addSeries(seriesStato);
                matricola.setText(getItem(position).getVoto());
            }
            else if (!getItem(position).getVoto().toLowerCase().contains("insufficiente")) {
                SeriesItem seriesStato = new SeriesItem.Builder(color)
                        .setRange(0, 32,8)
                        .build();



                SeriesItem seriesVoto = new SeriesItem.Builder(baseColor)
                        .setRange(0, 30,30)
                        .build();
                decoView.addSeries(seriesVoto);
                decoView.addSeries(seriesStato);

                matricola.setText(getItem(position).getVoto());
            }else{
                SeriesItem seriesStato = new SeriesItem.Builder(color)
                        .setRange(0, 32,8)
                        .build();



                SeriesItem seriesVoto = new SeriesItem.Builder(baseColor)
                        .setRange(0, 30,0)
                        .build();
                decoView.addSeries(seriesVoto);
                decoView.addSeries(seriesStato);
                matricola.setText("X");
            }







            corso.setText(getItem(position).getNomeEsame().substring(0, getItem(position).getNomeEsame().indexOf("-") - 1));
            corso.setTypeface(null, Typeface.BOLD);




            stato.setText("Scadenza: " + getItem(position).getDataChiusura());

            if(getItem(position).getStato() == 0){
                accetta.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onItemClick(getItem(position));
                    }
                });
            }




            return v;
        }

    }


}
