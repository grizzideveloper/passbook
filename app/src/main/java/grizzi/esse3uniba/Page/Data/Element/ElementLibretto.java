package grizzi.esse3uniba.Page.Data.Element;

import android.nfc.FormatException;
import android.os.Parcelable;

import org.jsoup.select.Elements;

/**
 * Created by Graziano on 05/02/2016.
 */
public class ElementLibretto extends Element{
    private String nomeCorso = "N/D";
    private String CFU = "N/D";
    private String votoEDataEsameSvolto = "N/D";

    public ElementLibretto(org.jsoup.nodes.Element row) throws FormatException{

        Elements cells = row.getElementsByTag("td");

        if(cells.size() >= 9){

            String courseName = cells.get(1).text();
            if(!courseName.isEmpty()){
                courseName = courseName.substring(courseName.indexOf("-") + 2);
                courseName = courseName.substring(0,1) + courseName.substring(1).toLowerCase();
                setNomeCorso(courseName);
            }else{
                throw new FormatException();//senza nome non posso inserire un esame
            }


            String courseCFU = cells.get(6).text();
            setCFU(courseCFU);

            String courseMarkDate = cells.get(9).text();
            setVotoEDataEsameSvolto(courseMarkDate);

        }else{
            throw new FormatException();
        }

    }



    public String getNomeCorso(){return nomeCorso;}
    public String getCFU(){return CFU;}
    public String getVotoEDataEsameSvolto(){
        return votoEDataEsameSvolto;
    }

    public void setNomeCorso(String nomeCorso){
        this.nomeCorso = nomeCorso;
    }
    public void setCFU(String CFU){
        this.CFU = CFU;
    }
    public void setVotoEDataEsameSvolto(String votoEDataEsameSvolto){
        this.votoEDataEsameSvolto = votoEDataEsameSvolto;
    }


}
