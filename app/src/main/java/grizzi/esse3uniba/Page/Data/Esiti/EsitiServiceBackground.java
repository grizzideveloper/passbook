package grizzi.esse3uniba.Page.Data.Esiti;

import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.IBinder;
import androidx.core.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import grizzi.esse3uniba.Esse3.Esse3Connection;
import grizzi.esse3uniba.Esse3.Esse3Url;
import grizzi.esse3uniba.Esse3.LoginListener;
import grizzi.esse3uniba.Esse3.User;
import grizzi.esse3uniba.Esse3.UserStore;
import grizzi.esse3uniba.Page.Data.Element.ElementVoti;
import grizzi.esse3uniba.Page.Data.Element.VoidElementException;
import grizzi.esse3uniba.Page.PageListener;

/**
 * Created by Graziano on 14/04/2016.
 */


public class EsitiServiceBackground extends Service {

    Esse3Connection connection;
    EsitiStore store;
    Context context = this;
    String name,password,carrieraID;
    NotificationCompat.Builder n;
    Thread backgroundThread;


    private Runnable myTask = new Runnable() {
        public void run() {
            task();
        }
    };

    private User user;
    private UserStore userStore;

    @Override
    public void onCreate() {
        this.context = this;
        this.backgroundThread = new Thread(myTask);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        //TODO do something useful
        connection = Esse3Connection.getInstance();
        store = new EsitiStore(context);

        try{

            userStore = UserStore.getInstance(this);

            try{
                user = userStore.getSavedUser();
                name = user.getName();
                password = user.getPassword();
                carrieraID = user.getId();
                this.backgroundThread.start();
            }catch (UserStore.NoUserFound e){
                stopSelf();
            }

        }catch (NullPointerException e){
            Toast.makeText(context,e.getMessage(),Toast.LENGTH_LONG).show();
            stopSelf();
        }
        return Service.START_STICKY;
    }

    private void update(){
            connection.getPage(Esse3Url.BACHECA_VOTI + "?" + "&" + carrieraID, new PageListener() {
                @Override
                public void onPageLoadDone(Document document) {

                    Elements tables = document.select("table");

                    for (int i = 3; i < tables.size(); i++) {

                        Elements detailsTables = tables.get(i).getElementsByTag("td");
                        Elements nameTables = tables.get(i).getElementsByTag("th");
                        try{
                            ElementVoti voto = new ElementVoti(detailsTables, nameTables);
                            if (!store.contains(voto)) {
                                store.add(voto);
                                n = new NotificationCompat.Builder(context)
                                        .setContentTitle("Nuovo voto")
                                        .setContentText(voto.getNomeEsame())
                                        .setSmallIcon(android.R.drawable.ic_dialog_email);

                                NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                                notificationManager.notify(0, n.build());
                            }
                        }catch(VoidElementException e){

                        }


                    }
                }

                @Override
                public void onPageLoadFailed(Exception e) {
                    Log.i("SERVICE", "Il service è in errore" + name + " " + password);
                }
            });

    }

    private void task(){


        while (true) {

            if(true){//dovresti controllare se ce la connessione a intenet

                if (!name.isEmpty() && !password.isEmpty()) {

                    if (connection.getConnectionState()) {
                        //Log.i("SERVICE", "Il login è già aperto " + name + " " + password);
                        //richiedo direttamente i dati
                        update();
                    } else {
                        //rifaccio login
                        //connection.stopConnection();
                        try {
                            connection.startConnection(new User(name, password), new LoginListener() {
                                @Override
                                public void onLoginFailed(Exception e) {
                                    //Log.i("SERVICE", "Il service è in errore" + name + " " + password);
                                }

                                @Override
                                public void onLoginDone(Document document) {
                                    //Log.i("SERVICE", "Login dal service riuscito " + name + " " + password);



                                    connection.getPage("https://www.studenti.ict.uniba.it/esse3/auth/studente/SceltaCarrieraStudente.do" + "?" + carrieraID, new PageListener() {
                                        @Override
                                        public void onPageLoadDone(Document document) {
                                            update();
                                        }

                                        @Override
                                        public void onPageLoadFailed(Exception e) {
                                            //Log.i("SERVICE", "Il service è in errore" + name + " " + password);

                                        }
                                    });
                                }
                            });
                        } catch (Exception e) {

                        }

                    }
                    //Log.i("SERVICE", "Il service è in running " + name + " " + password + " " + carrieraID);
                }
            }
            if(isOnline()){
                try{
                    Thread.sleep(21600000);
                }catch (Exception e){}
            }else{
                try{
                    Thread.sleep(300000);
                }catch (Exception e){}
            }

        }
    }

    public final boolean isOnline() {//funzione da spostare in connection
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED)
                        return true;
        }
        return false;
    }


    @Override
    public IBinder onBind(Intent intent) {
        //TODO for communication return IBinder implementation
        return null;
    }
}
