package grizzi.esse3uniba.Page.Data;


import android.content.Context;

import android.graphics.Typeface;


import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.TextView;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import grizzi.esse3uniba.Page.Data.Element.VoidElementException;
import grizzi.esse3uniba.R;
import grizzi.esse3uniba.Page.Data.Element.ElementPrenotazioni;

/**
 * Created by Graziano on 13/02/2016.
 */
public class DataPrenotazioni extends Data {
    private LinkedList<ElementPrenotazioni> listaVoci = new LinkedList<>();
    private PrenotazioniAdapter adapter;
    //private ExpandableListAdapter adapter;
    private HashMap<String,LinkedList<ElementPrenotazioni>> mappaVoci = new HashMap<>();
    private LinkedList<String> listaNomi = new LinkedList<>();
    Context context;


    public Boolean isEmpty() {
        return listaVoci.isEmpty();
    }


    public DataPrenotazioni(Context context){
        this.context =  context;
        adapter = new PrenotazioniAdapter(context, R.layout.libretto_row,listaVoci);
        //adapter = new ExpandableListAdapter(context,listaNomi,mappaVoci);
    }

    public void load(Document document){


        //per togliere eventuali tabelle vuote
        document.select("*[style=display:none]").remove();


        Elements tables = document.select("table");
        for(int i = 5;i < tables.size();i+=2){
            Element element = tables.get(i);
            Elements detailsTables = element.getElementsByTag("td");
            Elements form = element.select("form");

            Elements nameTable = element.getElementsByTag("th");
            if(!detailsTables.isEmpty() && !nameTable.isEmpty()){
                try{
                    listaVoci.add(new ElementPrenotazioni(detailsTables, nameTable,form));
                }catch (VoidElementException e){
                    continue;
                }


                //VocePrenotazioni nuovaVoce = new VocePrenotazioni(detailsTables,nameTable,form);
                //LinkedList<VocePrenotazioni> listaPrenotazioni = new LinkedList<>();
                //listaNomi.add(nuovaVoce.getNomeEsame());
                //listaPrenotazioni.add(nuovaVoce);
                //mappaVoci.put(nuovaVoce.getNomeEsame(),listaPrenotazioni);
            }
            adapter.notifyDataSetChanged();
        }
        setCacheValid(true);
    }

    @Override
    public void updateView(){
        adapter.notifyDataSetChanged();
    }



    public ElementPrenotazioni get(int position){
        return listaVoci.get(position);
    }

    public ListAdapter getAdapter(){
        return adapter;
    }

    public class PrenotazioniAdapter extends ArrayAdapter<ElementPrenotazioni> {

        public PrenotazioniAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        public PrenotazioniAdapter(Context context, int resource, List<ElementPrenotazioni> items) {
            super(context, resource, items);
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            View v = convertView;

            if (v == null) {
                LayoutInflater vi;
                vi = LayoutInflater.from(getContext());
                v = vi.inflate(R.layout.prenotazioni_row_new, null);
            }

            TextView data = (TextView)v.findViewById(R.id.textData);
            TextView corso = (TextView)v.findViewById(R.id.textNome);
            TextView posi = (TextView)v.findViewById(R.id.textPos);
            TextView info = (TextView)v.findViewById(R.id.info);
            TextView delete = (TextView)v.findViewById(R.id.delete);

            delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cancellaPrenotazione(getItem(position));
                }
            });

            info.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    stampDialog(getItem(position));
                }
            });

            data.setText(getItem(position).getData() + " - " + getItem(position).getOra());
            corso.setText(getItem(position).getNomeEsame().substring(0,getItem(position).getNomeEsame().indexOf("-")));
            posi.setText(getItem(position).getPosizione());

            return v;
        }

    }

    public void cancellaPrenotazione(ElementPrenotazioni prenotazione){}

    public void erease(){
        listaNomi.clear();
        listaVoci.clear();
        mappaVoci.clear();
    }

    public void stampDialog(ElementPrenotazioni prenotazione){}

    public class ExpandableListAdapter extends BaseExpandableListAdapter {

        private Context _context;
        private List<String> _listDataHeader; // header titles
        // child data in format of header title, child title
        private HashMap<String, LinkedList<ElementPrenotazioni>> _listDataChild;


        public ExpandableListAdapter(Context context, List<String> listDataHeader,
                                     HashMap<String, LinkedList<ElementPrenotazioni>> listChildData) {
            this._context = context;
            this._listDataHeader = listDataHeader;
            this._listDataChild = listChildData;
        }

        @Override
        public Object getChild(int groupPosition, int childPosititon) {
            return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .get(childPosititon);
        }

        @Override
        public long getChildId(int groupPosition, int childPosition) {
            return childPosition;
        }

        @Override
        public View getChildView(int groupPosition, final int childPosition,
                                 boolean isLastChild, View convertView, ViewGroup parent) {

            final ElementPrenotazioni child = (ElementPrenotazioni) getChild(groupPosition, childPosition);




            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this._context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.list_item_prenotazioni, null);
            }

            TextView data = (TextView) convertView
                    .findViewById(R.id.lblListItem);

            TextView ora  = (TextView) convertView
                    .findViewById(R.id.textView);

            TextView edificio  = (TextView) convertView
                    .findViewById(R.id.textView2);

            TextView aula  = (TextView) convertView
                    .findViewById(R.id.textView3);

            TextView docenti  = (TextView) convertView
                    .findViewById(R.id.textView4);

            TextView partizionamento  = (TextView) convertView
                    .findViewById(R.id.textView5);

            Button cancellaPrenotazione =(Button)convertView.findViewById(R.id.buttonCancella);

            cancellaPrenotazione.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    cancellaPrenotazione(child);
                }
            });


            data.setText("Data: " + child.getData());
            ora.setText("Ora: " + child.getOra());
            edificio.setText("Edificio: " + child.getEdificio());
            aula.setText("Aula: " + child.getAula());
            docenti.setText("Docenti: " + child.getDocenti());
            partizionamento.setText("Partizionamento: " + child.getRiservato());
            return convertView;
        }

        @Override
        public int getChildrenCount(int groupPosition) {
            return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                    .size();
        }

        @Override
        public Object getGroup(int groupPosition) {

            return this._listDataHeader.get(groupPosition);
        }

        @Override
        public int getGroupCount() {
            return _listDataHeader.size();
        }

        @Override
        public long getGroupId(int groupPosition) {
            /**/
            return groupPosition;
        }

        @Override
        public View getGroupView(int groupPosition, boolean isExpanded,
                                 View convertView, ViewGroup parent) {
            String headerTitle = (String) getGroup(groupPosition);
            if (convertView == null) {
                LayoutInflater infalInflater = (LayoutInflater) this._context
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = infalInflater.inflate(R.layout.list_group, null);
            }

            TextView lblListHeader = (TextView) convertView
                    .findViewById(R.id.lblListHeader);
            lblListHeader.setTypeface(null, Typeface.BOLD);
            lblListHeader.setText(headerTitle.substring(0,headerTitle.indexOf("-")-1));

            return convertView;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public boolean isChildSelectable(int groupPosition, int childPosition) {
            return true;
        }

        public void notifyDataSetInvalidated()
        {
            super.notifyDataSetInvalidated();
        }

    }
}
