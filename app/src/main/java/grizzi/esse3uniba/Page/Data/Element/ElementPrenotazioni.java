package grizzi.esse3uniba.Page.Data.Element;

import org.jsoup.select.Elements;

import java.util.LinkedList;

/**
 * Created by Graziano on 13/02/2016.
 */
public class ElementPrenotazioni extends Element {

    private String data;
    private String ora = "N/D";
    private String edificio = "N/D";
    private String aula = "N/D";
    private String riservato;
    private String docenti = "";
    private String nomeEsame;
    private String posPren = "N/D";
    private Boolean cancellabile;

    private LinkedList<Dettagli> dettagliPrenotazioni;





    public ElementPrenotazioni(Elements elements, Elements tableStruct, Elements form)throws VoidElementException{

        dettagliPrenotazioni = new LinkedList<>();

        if(elements.size() > 1){
            setData(elements.get(0).ownText());
            if(!elements.get(1).ownText().isEmpty())setOra(elements.get(1).ownText());
            if(!elements.get(2).ownText().isEmpty())setEdificio(elements.get(2).ownText());
            if(!elements.get(3).ownText().isEmpty())setAula(elements.get(3).ownText());
            setRiservato(elements.get(4).ownText());
            for(int i = 5;i<elements.size();i++){
                docenti = docenti + elements.get(i).ownText() + " ";
            }
            setNomeEsame(tableStruct.get(0).ownText());
            setPosizione(tableStruct.get(2).ownText());
        }

        if(form.size()>1){
            setCancellabile(true);
            Elements input = form.get(0).select("input");
            for(int z = 0;z < input.size();z++){
                dettagliPrenotazioni.add(new Dettagli(input.get(z).attr("name"),input.get(z).attr("value")));
            }
        }else{
            setCancellabile(false);
        }


    }

    public Boolean getCancellabile() {
        return cancellabile;
    }

    public void setCancellabile(Boolean cancellabile) {
        this.cancellabile = cancellabile;
    }

    public String getNomeEsame() {
        return nomeEsame;
    }

    public void setNomeEsame(String nomeEsame) {
        this.nomeEsame = nomeEsame;
    }

    public String getRiservato() {
        return riservato;
    }

    public void setRiservato(String riservato) {
        this.riservato = riservato;
    }

    public String getDocenti() {
        return docenti;
    }

    public void setDocenti(String docenti) {
        this.docenti = docenti;
    }


    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getOra() {
        return ora;
    }

    public void setOra(String ora) {
        this.ora = ora;
    }

    public String getAula() {
        return aula;
    }

    public void setAula(String aula) {
        this.aula = aula;
    }

    public String getPosizione() {
        return posPren;
    }

    public void setPosizione(String posPren) {
        this.posPren = posPren;
    }

    public String getEdificio() {
        return edificio;
    }

    public void setEdificio(String edificio) {
        this.edificio = edificio;
    }

    public String getId(){
        String id = "?";
        for(int i = 0;i < dettagliPrenotazioni.size();i++){
            id = id + dettagliPrenotazioni.get(i).getDettaglio();
        }

        return id;
    }
}
