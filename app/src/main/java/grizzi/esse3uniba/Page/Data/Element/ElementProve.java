package grizzi.esse3uniba.Page.Data.Element;

import org.jsoup.select.Elements;

import java.util.LinkedList;

/**
 * Created by Graziano on 05/02/2016.
 */
public class ElementProve extends Element {

    private String nomeAppello = "prova";
    private String data= "prova";
    private String descrizione= "prova";
    private String subscribedNum= "prova";

    private LinkedList<Dettagli> dettagliAppello;

    private Boolean prenotabile = false;





    private String imageFieldX;//forse non servono
    private String imageFieldY;//forse non servono



    public ElementProve(org.jsoup.nodes.Element row) throws VoidElementException{

        Elements cells = row.getElementsByTag("td");

        setNomeAppello(cells.get(1).text().trim());
        setData(cells.get(2).text().trim());
        setSubscribedNum(cells.get(3).text().trim());
        setDescrizione(cells.get(4).text().trim());


        dettagliAppello = new LinkedList<>();

        Elements elements = cells.get(0).getElementsByTag("input");
        if(elements.size() > 0){//set prenotabile
            setPrenotabile(true);

            for(int i = 0;i<elements.size();i++){
                dettagliAppello.add(new Dettagli(elements.get(i).attr("name"),elements.get(i).attr("value")));
            }
        }
    }


    public String getSubscribedNum() {
        return subscribedNum;
    }

    public void setSubscribedNum(String subscribedNum) {
        this.subscribedNum = subscribedNum;
    }

    public String getNomeAppello() {
        return nomeAppello;
    }

    public void setNomeAppello(String nomeAppello) {
        this.nomeAppello = nomeAppello;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }


    private Boolean getPrenotabile() {
        return prenotabile;
    }

    private void setPrenotabile(Boolean prenotabile) {
        this.prenotabile = prenotabile;
    }

    public Boolean prenotabile(){
        return prenotabile;
    }


    public String getId(){
        String id = "?";
        for(int i = 0;i < dettagliAppello.size();i++){
            id = id + dettagliAppello.get(i).getDettaglio();
        }

        return id;
    }

}
