package grizzi.esse3uniba.Page.Data.Esiti;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import androidx.core.app.NotificationCompat;
import android.util.Log;


import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import grizzi.esse3uniba.Esse3.Esse3Connection;
import grizzi.esse3uniba.Esse3.Esse3Url;
import grizzi.esse3uniba.Esse3.LoginListener;
import grizzi.esse3uniba.Esse3.User;
import grizzi.esse3uniba.Page.Data.Element.VoidElementException;
import grizzi.esse3uniba.Page.PageListener;
import grizzi.esse3uniba.Page.Data.Element.ElementVoti;

/**
 * Created by Graziano on 31/03/2016.
 */
public class EsitiService extends IntentService {

    Esse3Connection connection = Esse3Connection.getInstance();
    EsitiStore store;
    Context context = this;
    NotificationCompat.Builder n;

    public EsitiService(){
        super("EsitiService");
        store = new EsitiStore(context);
    }


    @Override
    protected void onHandleIntent(Intent intent){

        final String name,password,carrieraID;

        name = intent.getStringExtra("NAME");
        password = intent.getStringExtra("PASSWORD");
        carrieraID = intent.getStringExtra("ID_CARRIERA");

        while (true){

            if(!name.isEmpty() && !password.isEmpty()){

                if(connection.getConnectionState()){
                    //richiedo direttamente i dati
                    connection.getPage(Esse3Url.BACHECA_VOTI + "?" + carrieraID, new PageListener() {
                        @Override
                        public void onPageLoadDone(Document document) {
                            Elements tables = document.select("table");

                            for(int i = 3;i< tables.size();i++){
                                Elements detailsTables = tables.get(i).getElementsByTag("td");
                                Elements nameTables = tables.get(i).getElementsByTag("th");
                                try{
                                    ElementVoti voto = new ElementVoti(detailsTables, nameTables);
                                    if(!store.contains(voto)){
                                        store.add(voto);
                                        n   = new NotificationCompat.Builder(context)
                                                .setContentTitle("Nuovo voto")
                                                .setContentText(voto.getNomeEsame())
                                                .setSmallIcon(android.R.drawable.ic_dialog_email);

                                        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                                        notificationManager.notify(0, n.build());
                                    }
                                }catch(VoidElementException e){
                                    continue;
                                }

                            }
                        }

                        @Override
                        public void onPageLoadFailed(Exception e) {
                            Log.i("SERVICE","Il service è in errore" + name + " " + password);
                        }
                    });
                }else {
                    //rifaccio login
                    connection.stopConnection();
                    try{
                        connection.startConnection(new User(name,password), new LoginListener() {
                            @Override
                            public void onLoginFailed(Exception e) {
                                Log.i("SERVICE","Il service è in errore" + name + " " + password);
                            }

                            @Override
                            public void onLoginDone(Document document) {

                                connection.getPage(Esse3Url.BACHECA_VOTI + "?" + carrieraID, new PageListener() {
                                    @Override
                                    public void onPageLoadDone(Document document) {
                                        Elements tables = document.select("table");

                                        for(int i = 3;i< tables.size();i++){
                                            Elements detailsTables = tables.get(i).getElementsByTag("td");
                                            Elements nameTables = tables.get(i).getElementsByTag("th");
                                            try{
                                                ElementVoti voto = new ElementVoti(detailsTables, nameTables);
                                                if(!store.contains(voto)){
                                                    store.add(voto);
                                                    n   = new NotificationCompat.Builder(context)
                                                            .setContentTitle("Nuovo voto")
                                                            .setContentText(voto.getNomeEsame())
                                                            .setSmallIcon(android.R.drawable.ic_dialog_email);

                                                    NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                                                    notificationManager.notify(0, n.build());
                                                }
                                            }catch (VoidElementException e){
                                                continue;
                                            }

                                        }
                                    }

                                    @Override
                                    public void onPageLoadFailed(Exception e) {
                                        Log.i("SERVICE","Il service è in errore" + name + " " + password);

                                    }
                                });
                            }
                        });
                    }catch (Exception e){

                    }

                }

                Log.i("SERVICE","Il service è in running " + name + " " + password);

            }

            try{
                Thread.sleep(1000);
            }catch (Exception e){

            }


        }



    }



    @Override
    public void onDestroy()
    {

    }
}
