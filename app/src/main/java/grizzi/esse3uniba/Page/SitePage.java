package grizzi.esse3uniba.Page;

import android.os.Bundle;
import android.widget.Toast;

import grizzi.esse3uniba.Esse3.User;
import grizzi.esse3uniba.Page.Data.Data;
import grizzi.esse3uniba.Utils.CrashReport;


/**
 * Created by Graziano on 12/04/2016.
 */
public abstract class SitePage<T extends Data> extends Page<T> {

    @Override
    protected final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try{
            //Toast.makeText(context,"onCreate",Toast.LENGTH_LONG).show();
            setDataPool();

            setView();

            load();
        }catch (NullPointerException e){
            //Toast.makeText(context,e.getMessage(),Toast.LENGTH_LONG).show();
            CrashReport.reportException(e,this.getClass().getName());
        }
    }

    @Override
    protected User getUser(){
       return connection.getUser();
    }
}
