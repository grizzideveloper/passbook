package grizzi.esse3uniba.Page;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import androidx.core.app.NotificationCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;


import org.jsoup.nodes.Document;

import grizzi.esse3uniba.Esse3.Esse3Url;
import grizzi.esse3uniba.Esse3.UserStore;
import grizzi.esse3uniba.My.MyApplication;
import grizzi.esse3uniba.My.MySharedPreferences;
import grizzi.esse3uniba.Page.Data.DataMenu;

import grizzi.esse3uniba.R;
import grizzi.esse3uniba.SlidingTab.SlidingTabLayout;
import grizzi.esse3uniba.SlidingTab.ViewPageAdapter;
import grizzi.esse3uniba.Page.Data.Data;


public class PageMenu extends SitePage<DataMenu> {

    Toolbar toolbar;
    ViewPager pager;
    ViewPageAdapter adapter;
    SlidingTabLayout tabs;
    Context context = this;
    int Numboftabs =4;
    MySharedPreferences preferences;
    CharSequence Titles[]={"Home","Esami","Esiti","Orario"};





    @Override
    protected String getUrl(){
        return Esse3Url.HOME_URL;
    }

    @Override
    protected Data initDataPool(){
        return new DataMenu(context);
    }


    @Override
    public void onPageLoaded(){
        toolbar.setTitle("PassBook Mat." + getDataPool().getMatricola());
    }

    @Override
    public void onPageLoadError(){
        super.onPageLoadError();
    }



    @Override
    public void setView(){
        setContentView(R.layout.menu);




        preferences = MySharedPreferences.getInstance(context,"NEWS");
        int alreadyDone = preferences.getInt("WIDGET",0);
        if(alreadyDone == 0){

            preferences.putInt("WIDGET",1);


            AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
            builder1.setMessage("Da oggi è stato aggiunto un widget per poter tenere sempre sullo schermo l'orario delle lezioni.");
            builder1.setCancelable(false);

            builder1.setPositiveButton(
                    "Fantastico!",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {

                            dialog.cancel();
                        }
                    });


            AlertDialog alert11 = builder1.create();
            alert11.show();
        }
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("PassBook");

        toolbar.setNavigationIcon(R.drawable.logosimin);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        setSupportActionBar(toolbar);
        setTabs();
    }

    private void setTabs(){
        // Creating The ViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
        adapter =  new ViewPageAdapter(getSupportFragmentManager(),Titles,Numboftabs);

        // Assigning ViewPager View and setting the adapter
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setOffscreenPageLimit(3);
        pager.setAdapter(adapter);

        // Assiging the Sliding Tab Layout View
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

        // Setting Custom Color for the Scroll bar indicator of the Tab View
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.tabsScrollColor);
            }
        });

        // Setting the ViewPager For the SlidingTabsLayout
        tabs.setViewPager(pager);
    }

    @Override
    public void onBackPressed(){
        Intent i = new Intent(Intent.ACTION_MAIN);
        i.addCategory(Intent.CATEGORY_HOME);
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add("Logout").setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                if(isOnline()){
                    final ProgressDialog ringProgressDialog =  ProgressDialog.show(context, "Attendere ...", "Chiusua in corso ...", true);
                    connection.getPage("https://www.studenti.ict.uniba.it/esse3/Logout.do", new PageListener() {
                        @Override
                        public void onPageLoadDone(Document document) {
                            connection.stopConnection();
                            MyApplication app = MyApplication.getInstance();
                            app.clearCache();
                            UserStore uStore = UserStore.getInstance(context);
                            uStore.clear();
                            startActivity(new Intent(context,LoginActivity.class));
                            ringProgressDialog.dismiss();
                        }

                        @Override
                        public void onPageLoadFailed(Exception e) {
                            ringProgressDialog.dismiss();
                        }
                    });
                }else {
                    Toast.makeText(context,"Internet assente",Toast.LENGTH_LONG).show();
                }
                return false;
            }
        });
        return true;
    }
}
