package grizzi.esse3uniba.Page;

import android.content.Intent;
import androidx.appcompat.widget.Toolbar;
import android.widget.ListView;

import grizzi.esse3uniba.Esse3.Esse3Url;
import grizzi.esse3uniba.Page.Data.Data;
import grizzi.esse3uniba.Page.Data.DataCarriera;
import grizzi.esse3uniba.R;


public class PageCarriere extends SitePage<DataCarriera> {

    Toolbar toolbar;
    ListView careerList;

    @Override
    protected String getUrl(){
        return Esse3Url.LOGIN_URL;
    }

    @Override
    protected void setView(){
        setContentView(R.layout.activity_choosecareer);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("UniBa PassBook");
        toolbar.setTitleTextColor(0xFFFFFFFF);

        careerList = (ListView)findViewById(R.id.listCareer);
        careerList.setAdapter(getDataPool().getAdapter());

        setSupportActionBar(toolbar);
    }

    @Override
    protected Data initDataPool(){
        return new DataCarriera(this){
            @Override
            public void onClick(){
                Intent i = new Intent(context, NewMainActivity.class);
                context.startActivity(i);
                finish();
            }
        };
    }

    @Override
    public void onPageLoaded(){


    }



    @Override
    public void onBackPressed(){

    }

    @Override
    protected boolean mustSaveCache() {
        return false;
    }
}
