package grizzi.esse3uniba.Page;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.graphics.Color;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.provider.ContactsContract;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.an.biometric.BiometricCallback;
import com.an.biometric.BiometricManager;

import java.util.List;

import grizzi.esse3uniba.Esse3.User;
import grizzi.esse3uniba.My.MyApplication;
import grizzi.esse3uniba.Page.Data.Data;
import grizzi.esse3uniba.Page.Data.DataLogin;
import grizzi.esse3uniba.R;


/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends StartingPage /*implements LoaderCallbacks<Cursor> */{
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////
    private CheckBox checkSalvaDati;
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;



    @Override
    protected void lastUserFounded(){
        mEmailView.setText(getUser().getName());
        mPasswordView.setText(getUser().getPassword());
        checkSalvaDati.setChecked(true);
        if(isOnline()){
            showProgress(true);
            load();
        }
    }

    @Override
    protected void lastUserUnfounded(){
        checkSalvaDati.setChecked(false);
    }

    @Override
    protected  void multiCarrer(){
        Intent i = new Intent(getBaseContext(), PageCarriere.class);
        startActivity(i);
        finish();
    }

    @Override
    protected  void singleCarrer(){
        Intent i = new Intent(getBaseContext(), NewMainActivity.class);
        startActivity(i);
        finish();
    }


    @Override
    protected boolean mustSaveUser(){
        if(checkSalvaDati.isChecked())return true;
        else return false;
    }

    @Override
    protected Data initDataPool(){
        return new DataLogin();
    }

    @Override
    protected void onPageLoaded(){
        showProgress(false);
    }

    @Override
    public void onPageLoadError(){
        showProgress(false);
        mPasswordView.setError(getString(R.string.error_incorrect_password));
        mPasswordView.requestFocus();
    }

    @Override
    protected void setView(){
        setContentView(R.layout.new_activity_login);
        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);
        mEmailView.setHintTextColor(Color.parseColor("#ffffff"));
        populateAutoComplete();

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setHintTextColor(Color.parseColor("#ffffff"));
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        if(connection.getConnectionState()){
            connection.stopConnection();
            MyApplication app = MyApplication.getInstance();
            app.clearCache();
        }


        checkSalvaDati = (CheckBox)findViewById(R.id.checkBox);

    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "foo@example.com:hello", "bar@example.com:world"
    };
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
   

    // UI references.




    private void populateAutoComplete() {
        //getLoaderManager().initLoader(0, null, this);
    }


    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {

        // Reset errors.
        mEmailView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            try{
                setUser(new User(mEmailView.getText().toString(),mPasswordView.getText().toString()));
                load();
            }catch (User.EmptyFieldException e){
                Toast.makeText(context,"Campo password o nick vuoti",Toast.LENGTH_LONG).show();
            }
        }
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return true;
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    /*
    @Override
    public Loader<Cursor> onCreateLoader(int i, Bundle bundle) {
        return new CursorLoader(this,
                // Retrieve data rows for the device user's 'profile' contact.
                Uri.withAppendedPath(ContactsContract.Profile.CONTENT_URI,
                        ContactsContract.Contacts.Data.CONTENT_DIRECTORY), ProfileQuery.PROJECTION,

                // Select only email addresses.
                ContactsContract.Contacts.Data.MIMETYPE +
                        " = ?", new String[]{ContactsContract.CommonDataKinds.Email
                .CONTENT_ITEM_TYPE},

                // Show primary email addresses first. Note that there won't be
                // a primary email address if the user hasn't specified one.
                ContactsContract.Contacts.Data.IS_PRIMARY + " DESC");
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        List<String> emails = new ArrayList<>();
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            emails.add(cursor.getString(ProfileQuery.ADDRESS));
            cursor.moveToNext();
        }

        addEmailsToAutoComplete(emails);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> cursorLoader) {

    }*/

    private void addEmailsToAutoComplete(List<String> emailAddressCollection) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, emailAddressCollection);

        mEmailView.setAdapter(adapter);
    }


    private interface ProfileQuery {
        String[] PROJECTION = {
                ContactsContract.CommonDataKinds.Email.ADDRESS,
                ContactsContract.CommonDataKinds.Email.IS_PRIMARY,
        };

        int ADDRESS = 0;
        int IS_PRIMARY = 1;
    }

    private void showFingerPrint(){
        new BiometricManager.BiometricBuilder(this)
                .setTitle("Add a title")
                .setSubtitle("Add a subtitle")
                .setDescription("Add a description")
                .setNegativeButtonText("Add a cancel button")
                .build()
                .authenticate(new BiometricCallback() {
                    @Override
                    public void onSdkVersionNotSupported() {
                        System.out.println();
                    }

                    @Override
                    public void onBiometricAuthenticationNotSupported() {
                        System.out.println();
                    }

                    @Override
                    public void onBiometricAuthenticationNotAvailable() {
                        System.out.println();
                    }

                    @Override
                    public void onBiometricAuthenticationPermissionNotGranted() {
                        System.out.println();
                    }

                    @Override
                    public void onBiometricAuthenticationInternalError(String error) {
                        System.out.println();
                    }

                    @Override
                    public void onAuthenticationFailed() {
                        System.out.println();
                    }

                    @Override
                    public void onAuthenticationCancelled() {
                        System.out.println();
                    }

                    @Override
                    public void onAuthenticationSuccessful() {
                        System.out.println();
                    }

                    @Override
                    public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
                        System.out.println();
                    }

                    @Override
                    public void onAuthenticationError(int errorCode, CharSequence errString) {
                        System.out.println();
                    }
                });
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    @Override
    protected void onResume() {
        super.onResume();
    }
}

