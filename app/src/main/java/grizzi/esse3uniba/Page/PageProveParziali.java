package grizzi.esse3uniba.Page;


import android.content.Context;
import android.content.Intent;
import androidx.core.view.MenuItemCompat;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.AbsListView;
import android.widget.ExpandableListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import grizzi.esse3uniba.Esse3.Esse3Url;
import grizzi.esse3uniba.Page.Data.Data;
import grizzi.esse3uniba.R;
import grizzi.esse3uniba.Page.Data.DataProveParziali;
import grizzi.esse3uniba.Page.Data.Element.ElementProve;


/**
 * Created by Graziano on 05/02/2016.
 */
public class PageProveParziali extends SitePage<DataProveParziali> {

    Toolbar toolbar;
    Context context =  this;
    ExpandableListView listaAppelli;
    ProgressBar progressBar;


    @Override
    protected String getUrl(){
        return Esse3Url.PROVE_URL;
    }

    @Override
    protected Data initDataPool(){
        return new DataProveParziali(context){
            @Override
            public void onItemClick(final ElementProve voce){
                if(voce.prenotabile()){
                    if(isOnline()){
                        Intent i = new Intent(getBaseContext(), PagePrenotazioneProve.class);
                        i.putExtra("ID_ESAME", voce.getId());
                        startActivity(i);
                    }else{
                        Toast.makeText(context,"Internet assente", Toast.LENGTH_LONG).show();
                    }
                }
            }
        };
    }



    @Override
    protected void setView(){
        setContentView(R.layout.activity_prove_parziali);

        progressBar = (ProgressBar)findViewById(R.id.progressBar);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("Prove parziali");
        toolbar.setTitleTextColor(0xFFFFFFFF);

        listaAppelli = (ExpandableListView)findViewById(R.id.listAppelli);

        View footerView = View.inflate(this, R.layout.footer_blanc, null);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        listaAppelli.setOnScrollListener(new AbsListView.OnScrollListener() {
            int mLastFirstVisibleItem = 0;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                //if (view.getId() == listaAppelli.getId()) {
                final int currentFirstVisibleItem = listaAppelli.getFirstVisiblePosition();

                if (currentFirstVisibleItem > mLastFirstVisibleItem) {
                    // getSherlockActivity().getSupportActionBar().hide();
                    toolbar.animate().translationY(-toolbar.getBottom()).setInterpolator(new AccelerateInterpolator()).start();

                } else if (currentFirstVisibleItem < mLastFirstVisibleItem) {
                    // getSherlockActivity().getSupportActionBar().show();
                    toolbar.animate().translationY(0).setInterpolator(new DecelerateInterpolator()).start();

                }

                mLastFirstVisibleItem = currentFirstVisibleItem;
                //}
            }
        });
        listaAppelli.addFooterView(footerView);
        listaAppelli.setAdapter(getDataPool().getExpandableAdapter());
    }

    @Override
    public void onPageLoaded(){

        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onPageLoadError(){
        super.onPageLoadError();
        progressBar.setVisibility(View.GONE);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_search, menu);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                getDataPool().setFilter(newText);
                return false;
            }
        });


        return true;
    }
}
