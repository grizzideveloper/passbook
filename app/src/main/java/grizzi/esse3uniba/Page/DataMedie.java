package grizzi.esse3uniba.Page;

import grizzi.esse3uniba.Page.Data.Data;

/**
 * Created by grazi on 27/04/2017.
 */

public class DataMedie  {
    private String ponderata = "0";
    private String aritmetica = "0";

    public DataMedie(String ponderata,String aritmetica){
        this.ponderata = ponderata;
        this.aritmetica = aritmetica;
    }

    public String getPonderata() {
        return ponderata;
    }

    public String getAritmetica() {
        return aritmetica;
    }

    public void setPonderata(String ponderata) {
        this.ponderata = ponderata;
    }

    public void setAritmetica(String aritmetica) {
        this.aritmetica = aritmetica;
    }
}
