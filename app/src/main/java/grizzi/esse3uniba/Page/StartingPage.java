package grizzi.esse3uniba.Page;


import android.os.Bundle;
import android.widget.Toast;

import org.jsoup.nodes.Document;

import java.nio.channels.AlreadyConnectedException;

import grizzi.esse3uniba.Esse3.Esse3Url;
import grizzi.esse3uniba.Esse3.LoginListener;
import grizzi.esse3uniba.Esse3.User;
import grizzi.esse3uniba.Esse3.UserStore;
import grizzi.esse3uniba.Page.Data.DataLogin;
import grizzi.esse3uniba.Utils.CrashReport;

/**
 * Created by Graziano on 05/04/2016.
 */
public abstract class StartingPage extends Page<DataLogin> {

    protected abstract void lastUserFounded();
    protected abstract void lastUserUnfounded();
    protected abstract void multiCarrer();
    protected abstract void singleCarrer();
    protected abstract boolean mustSaveUser();

    private User user;
    private UserStore userStore;

    @Override
    protected final void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try{
            setDataPool();

            setView();

            userStore = UserStore.getInstance(this);

            try{
                user = userStore.getSavedUser();
                lastUserFounded();
            }catch (UserStore.NoUserFound e){
                lastUserUnfounded();
            }

        }catch (NullPointerException e){
            Toast.makeText(context,e.getMessage(),Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void load(){//dovremmo mettere un controllo di settaggio dell'user che magari è vuoto
        if(isOnline()){
            if(!connection.getConnectionState()){

                try{

                    connection.startConnection(getUser(), new LoginListener() {

                        @Override
                        public void onLoginDone(Document document) {

                            if(mustSaveUser()){
                                userStore.saveUser(user);
                            }else {
                                userStore.saveUser(null);//questo cancella l'user
                            }

                            if(document != null){
                                try{
                                    getDataPool().load(document);

                                    onPageLoaded();

                                    if (getDataPool().isMultiCarrer()) {
                                        //user.setMultipleCarrer(true);
                                        multiCarrer();
                                    } else {
                                        //user.setMultipleCarrer(false);
                                        singleCarrer();
                                    }
                                }catch (Exception e){
                                    CrashReport.reportException(e,getDataPool().getClass().getName());
                                    printMsg("Errore nel caricamento della pagina", context, Toast.LENGTH_LONG);
                                    onPageLoadError();
                                }
                            }else{
                                printMsg("Errore nel caricamento della pagina", context, Toast.LENGTH_LONG);
                                onPageLoadError();
                            }
                        }

                        @Override
                        public void onLoginFailed(Exception e) {
                            printMsg("Password o nickname errati", context, Toast.LENGTH_LONG);
                            onPageLoadError();
                        }

                    });

                }catch (AlreadyConnectedException e){

                }catch (NullPointerException e1){
                    Toast.makeText(this,"Login listener null pointer exception",Toast.LENGTH_LONG).show();
                }
            }else {
                /*if(connection.getUser().isMultipleCarrer()){
                    multiCarrer();
                }else singleCarrer();*/

                /*
                connection.getPage("https://www.studenti.ict.uniba.it/esse3/Logout.do", new PageListener() {
                    @Override
                    public void onPageLoadDone(Document document) {
                        connection.stopConnection();
                        load();
                    }

                    @Override
                    public void onPageLoadFailed(Exception e) {

                    }
                });*/
                connection.stopConnection();
                load();
            }

        }else{
            Toast.makeText(this,"Internet assente",Toast.LENGTH_LONG).show();
            onPageLoadError();
        }
    }

    @Override
    protected String getUrl(){
        return Esse3Url.LOGIN_URL;
    }


    protected final void setUser(User user)throws NullPointerException{
        if(user != null){
            this.user = user;
        }else throw new NullPointerException("Riferimento a user è null");
    }

    protected final User getUser(){
        return user;
    }

    @Override
    protected void onResume() {
        super.onResume();
    }
}
