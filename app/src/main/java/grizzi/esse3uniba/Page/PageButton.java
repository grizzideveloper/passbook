package grizzi.esse3uniba.Page;

import android.content.Context;
import android.view.View;
import android.widget.Button;

import org.jsoup.nodes.Document;

import grizzi.esse3uniba.Esse3.Esse3Connection;

/**
 * Created by Graziano on 14/04/2016.
 */
public abstract class PageButton {

    private Button myButton;
    private String myUrl;

    private Esse3Connection connection = Esse3Connection.getInstance();

    protected abstract void onError();
    protected abstract void onDone(Document document);

    PageButton(Context context,Button newButton, String url){
        setMyUrl(url);
        setMyButton(newButton);

        getMyButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                connection.getPage(getMyUrl(), new PageListener() {
                    @Override
                    public void onPageLoadDone(Document document) {
                        onDone(document);
                    }

                    @Override
                    public void onPageLoadFailed(Exception e) {
                        onError();
                    }
                });
            }
        });
    }


    public Button getMyButton() {
        return myButton;
    }

    public void setMyButton(Button myButton) {
        this.myButton = myButton;
    }

    public String getMyUrl() {
        return myUrl;
    }

    public void setMyUrl(String myUrl) {
        this.myUrl = myUrl;
    }
}
