package grizzi.esse3uniba.Page;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.jsoup.nodes.Document;

import grizzi.esse3uniba.Page.Data.DataDettagliAppello;
import grizzi.esse3uniba.Esse3.Esse3Connection;
import grizzi.esse3uniba.Esse3.Esse3Url;
import grizzi.esse3uniba.My.MyApplication;
import grizzi.esse3uniba.Page.Data.Data;
import grizzi.esse3uniba.R;

/**
 * Created by Graziano on 10/02/2016.
 */
public class PagePrenotazioneProve extends SitePage<DataDettagliAppello> {

    Toolbar toolbar;
    Context context =  this;
    Button prenota;
    TextView attivita,appello,sessioni,tipoEsame,docenti;
    String idEsame;


    @Override
    public String getUrl(){
        return Esse3Url.PRENOTAZIONE_URL + idEsame;
    }

    @Override
    protected Data initDataPool(){
        return new DataDettagliAppello(context);
    }

    @Override
    protected void setView(){
        setContentView(R.layout.activity_prenotazione);

        Bundle extras = getIntent().getExtras();
        idEsame = extras.getString("ID_ESAME");

        prenota = (Button)findViewById(R.id.buttonPrenotazione);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("Prenotazione esame");
        toolbar.setTitleTextColor(0xFFFFFFFF);

        attivita = (TextView)findViewById(R.id.textAttivita);
        appello = (TextView)findViewById(R.id.textAppello);
        sessioni = (TextView)findViewById(R.id.textSessioni);
        tipoEsame = (TextView)findViewById(R.id.textTipoEsame);
        docenti = (TextView)findViewById(R.id.textDocenti);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        prenota.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                booking();
            }
        });
    }

    @Override
    public void onPageLoaded(){
        attivita.setText(getDataPool().getAttivita());
        sessioni.setText("Sessioni: " + getDataPool().getSessioni());
        appello.setText("Appello: " + getDataPool().getAppello());
        docenti.setText("Docente/i: " + getDataPool().getDocenti());
        tipoEsame.setText("Tipo: " + getDataPool().getTipoEsame());
    }

    private void booking(){
        final ProgressDialog ringProgressDialog = ProgressDialog.show(context, "Attendere ...", "Caricamento dati ...", true);
        Esse3Connection connection = Esse3Connection.getInstance();
        connection.getPage(Esse3Url.CONFERMA_PRENOTAZIONE_URL + idEsame, new PageListener() {
            @Override
            public void onPageLoadDone(Document document) {
                ringProgressDialog.dismiss();
                if (document.html().toLowerCase().contains("prenotazione effettuata")) {
                    MyApplication app = MyApplication.getInstance();
                    //if(!app.proveParzialiIsVoid())app.getDataPoolProveParziali().invalidateCache();
                    Toast.makeText(getApplicationContext(), "Esame prenotato", Toast.LENGTH_LONG).show();
                    app.deleteCache(Esse3Url.PROVE_URL);
                    Intent i = new Intent(getApplicationContext(), PageMenu.class);
                    startActivity(i);
                } else {
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(PagePrenotazioneProve.this);
                    builder1.setMessage("Impossibile effettuare la prenotazione\ncompilare prima il questionario");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });


                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }

            }

            @Override
            public void onPageLoadFailed(Exception e) {
                ringProgressDialog.dismiss();
                onPageLoadError();
            }
        });
    }
}
