package grizzi.esse3uniba.Page;


import android.content.Context;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;

import grizzi.esse3uniba.Esse3.Esse3Url;
import grizzi.esse3uniba.My.MyApplication;
import grizzi.esse3uniba.Page.Data.Data;
import grizzi.esse3uniba.R;
import grizzi.esse3uniba.Page.Data.DataPagamenti;


/**
 * Created by Graziano on 05/02/2016.
 */
public class PagePagamenti extends SitePage<DataPagamenti> {



    Toolbar toolbar;
    Context context =  this;
    MyApplication app;
    ListView listaPagamenti;
    ProgressBar progressBar;

    @Override
    protected String getUrl(){
        return Esse3Url.PAGAMENTI;
    }

    @Override
    protected Data initDataPool(){
        return new DataPagamenti(context);
    }


    @Override
    protected void setView(){
        setContentView(R.layout.activity_pagamenti);

        progressBar = (ProgressBar)findViewById(R.id.progressBar);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("Pagamenti");
        toolbar.setTitleTextColor(0xFFFFFFFF);

        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        listaPagamenti = (ListView)findViewById(R.id.listLibretto);
        listaPagamenti.setAdapter(getDataPool().getAdapter());
    }

    @Override
    public void onPageLoaded(){

        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void onPageLoadError(){
        super.onPageLoadError();
        progressBar.setVisibility(View.GONE);
    }

}
