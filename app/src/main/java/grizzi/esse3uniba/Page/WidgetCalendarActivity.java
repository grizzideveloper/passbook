package grizzi.esse3uniba.Page;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.core.app.NotificationCompat;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;

import grizzi.esse3uniba.R;
import grizzi.esse3uniba.SlidingTab.SlidingTabLayout;
import grizzi.esse3uniba.SlidingTab.ViewCalendarAdapter;

/**
 * Created by grazi on 31/03/2017.
 */

public class WidgetCalendarActivity extends AppCompatActivity {


    Toolbar toolbar;
    ViewPager pager;
    ViewCalendarAdapter adapter;
    SlidingTabLayout tabs;
    Context context = this;
    int Numboftabs =5;
    CharSequence Titles[]={"Lun","Mar","Mer","Gio","Ven"};
    FloatingActionButton fab;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calendar_activity);


        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("PassBook Calendar");

        toolbar.setNavigationIcon(R.drawable.logosimin);
        toolbar.setTitleTextColor(0xFFFFFFFF);
        setSupportActionBar(toolbar);

        // Creating The ViewPagerAdapter and Passing Fragment Manager, Titles fot the Tabs and Number Of Tabs.
        adapter =  new ViewCalendarAdapter(getSupportFragmentManager(),Titles,Numboftabs);

        // Assigning ViewPager View and setting the adapter
        pager = (ViewPager) findViewById(R.id.pagerFr);
        pager.setOffscreenPageLimit(5);
        pager.setAdapter(adapter);

        // Assiging the Sliding Tab Layout View
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

        // Setting Custom Color for the Scroll bar indicator of the Tab View
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return getResources().getColor(R.color.tabsScrollColor);
            }
        });

        // Setting the ViewPager For the SlidingTabsLayout
        tabs.setViewPager(pager);

        fab = (FloatingActionButton)findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(context,AddEventActivity.class));
            }
        });


    }

}
