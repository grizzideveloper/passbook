package grizzi.esse3uniba.Page;

/**
 * Created by Graziano on 21/03/2016.
 */
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import grizzi.esse3uniba.My.Event;
import grizzi.esse3uniba.My.MyCalendar.MyCalendar;
import grizzi.esse3uniba.My.MyCalendar.MyWidgetProvider;
import grizzi.esse3uniba.R;

public class AddEventActivity extends AppCompatActivity {

    Toolbar toolbar;
    Spinner daySpinner;
    String[] day = {"Lunedì","Martedì","Mercoledì","Giovedì","Venerdì"};
    FloatingActionButton fab;
    EditText courseName;
    Context context = this;
    TimePicker inizio,fine;

    MyCalendar calendar = MyCalendar.getCalendar();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_event);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setTitle("Appelli");
        toolbar.setTitleTextColor(0xFFFFFFFF);

        daySpinner = (Spinner)findViewById(R.id.spinner);
        daySpinner.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item,day));

        courseName = (EditText)findViewById(R.id.textNomeCorso);

        inizio = (TimePicker)findViewById(R.id.timePicker);
        fine = (TimePicker)findViewById(R.id.timePicker2);

        fab = (FloatingActionButton)findViewById(R.id.fab);





        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(courseName.getText().toString().isEmpty()){
                    Toast.makeText(context,"Errore, nome corso mancante",Toast.LENGTH_LONG).show();
                }else {
                    String oraInizio = "",oraFine = "";
                    if(inizio.getCurrentMinute()==0){
                        oraInizio = inizio.getCurrentHour() + ":" + "00";
                    }else{
                        oraInizio = inizio.getCurrentHour() + ":" + inizio.getCurrentMinute();
                    }
                    if(fine.getCurrentMinute()==0){
                        oraFine = fine.getCurrentHour() + ":" + "00";
                    }else{
                        oraFine = fine.getCurrentHour() + ":" + fine.getCurrentMinute();
                    }
                    calendar.addEvent(daySpinner.getSelectedItemPosition(),
                            new Event(courseName.getText().toString(),
                                    oraInizio,
                                    oraFine));
                    Intent intent = new Intent(getApplicationContext(), MyWidgetProvider.class);
                    intent.setAction("lun");
                    int ids[] = AppWidgetManager.getInstance(getApplication()).getAppWidgetIds(new ComponentName(getApplication(), MyWidgetProvider.class));
                    intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS,ids);
                    sendBroadcast(intent);
                    finish();
                }
            }
        });




    }
}
