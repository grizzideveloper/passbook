package grizzi.esse3uniba.viewPager.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.florent37.materialviewpager.header.MaterialViewPagerHeaderDecorator;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import grizzi.esse3uniba.Page.Data.Element.ElementLibretto;
import grizzi.esse3uniba.Page.DataMedie;
import grizzi.esse3uniba.R;
import grizzi.esse3uniba.viewPager.TestRecyclerViewAdapter;

/**
 * Created by florentchampigny on 24/04/15.
 */
public class RecyclerViewFragmentLibretto extends Fragment {

    private static final boolean GRID_LAYOUT = false;
    private static final int ITEM_COUNT = 100;
    private ArrayList<ElementLibretto> esami;
    private DataMedie medie;

    TestRecyclerViewAdapter  testRecyclerViewAdapter =  null;

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;


    public static RecyclerViewFragmentLibretto newInstance(DataMedie medie, ArrayList<ElementLibretto> esami) {
        RecyclerViewFragmentLibretto recyclerViewFragmentLibretto = new RecyclerViewFragmentLibretto();
        recyclerViewFragmentLibretto.setMedie(medie);
        recyclerViewFragmentLibretto.setEsami(esami);
        return recyclerViewFragmentLibretto;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_recyclerview, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        final List<Object> items = new ArrayList<>();

        for (int i = 0; i < ITEM_COUNT; ++i) {
            items.add(new Object());
        }

        if (GRID_LAYOUT) {
            mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        } else {
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        }
        mRecyclerView.setHasFixedSize(true);

        //Use this now
        mRecyclerView.addItemDecoration(new MaterialViewPagerHeaderDecorator());
        testRecyclerViewAdapter = new TestRecyclerViewAdapter(esami,getMedie());


        mRecyclerView.setAdapter(testRecyclerViewAdapter);
    }

    public ArrayList<ElementLibretto> getEsami() {
        return esami;
    }

    private void setEsami(ArrayList<ElementLibretto> esami) {
        this.esami = esami;
    }

    public DataMedie getMedie() {
        return medie;
    }

    public void setMedie(DataMedie medie) {
        this.medie = medie;
    }

    public void update(){
        testRecyclerViewAdapter.update();
    }
}
