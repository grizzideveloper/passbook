package grizzi.esse3uniba.viewPager;

import android.graphics.Color;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar;

import java.util.ArrayList;

import grizzi.esse3uniba.Page.Data.Element.ElementLibretto;
import grizzi.esse3uniba.Page.DataMedie;
import grizzi.esse3uniba.R;

/**
 * Created by florentchampigny on 24/04/15.
 */
public class TestRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<ElementLibretto>  contents;
    private DataMedie medie;
    private View firstView = null;

    static final int TYPE_HEADER = 0;
    static final int TYPE_CELL = 1;

    public TestRecyclerViewAdapter(ArrayList<ElementLibretto> contents,DataMedie medie) {
        this.contents = contents;
        this.medie = medie;
    }

    @Override
    public int getItemViewType(int position) {
        switch (position) {
            case 0:
                return TYPE_HEADER;
            default:
                return position;
        }
    }

    @Override
    public int getItemCount() {
        if(contents == null)return 0;
        return contents.size() + 1;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;

        if(viewType==0) {
            if(firstView==null){
                firstView = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_item_card_big, parent, false);

                RoundCornerProgressBar progress2 = (RoundCornerProgressBar) firstView.findViewById(R.id.progressBar);
                progress2.setProgressColor(Color.parseColor("#3F51B5"));
                progress2.setProgressBackgroundColor(Color.parseColor("#eeeeee"));
                progress2.setMax(180);
                progress2.setProgress(75);
                TextView ponderata = (TextView)firstView.findViewById(R.id.ponderataText);
                TextView aritmetica = (TextView)firstView.findViewById(R.id.aritmeticaText);
                ponderata.setText(medie.getPonderata());
                aritmetica.setText(medie.getAritmetica());
                return new RecyclerView.ViewHolder(firstView) {
                };
            }else {
                return new RecyclerView.ViewHolder(firstView) {
                };
            }

            }
            else  {
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_item_card_small, parent, false);
                int position = viewType-1;

                TextView matricola = (TextView)view.findViewById(R.id.textCFU);
                TextView corso = (TextView)view.findViewById(R.id.textNomeCorso);
                TextView stato = (TextView)view.findViewById(R.id.textStato);
                TextView data = (TextView)view.findViewById(R.id.textData);

                View votol = (View) view.findViewById(R.id.layout_voto);

                corso.setText(contents.get(position).getNomeCorso());


                int voto =0;


/////////////////////////////////////////////////////////////////////////////////////////////////////

                try{
                    if(contents.get(position).getVotoEDataEsameSvolto().isEmpty()){
                        stato.setText("CFU: " + contents.get(position).getCFU());
                        matricola.setText("N/D");
                        votol.setVisibility(View.GONE);
                    }else if(!contents.get(position).getVotoEDataEsameSvolto().substring(0,1).equals("I") && contents.get(position).getVotoEDataEsameSvolto().length() > 5){
                        stato.setText("CFU: " + contents.get(position).getCFU());
                        voto = Integer.parseInt(contents.get(position).getVotoEDataEsameSvolto().substring(0,2));
                        matricola.setText(contents.get(position).getVotoEDataEsameSvolto().substring(0,contents.get(position).getVotoEDataEsameSvolto().indexOf("-")-1));//??
                        data.setText(contents.get(position).getVotoEDataEsameSvolto().substring(contents.get(position).getVotoEDataEsameSvolto().indexOf("-")+2));
                    }else if(contents.get(position).getVotoEDataEsameSvolto().length() > 5) {
                        stato.setText("CFU: " + contents.get(position).getCFU());
                        matricola.setText("I");
                        voto = 30;
                    }
                }catch (NumberFormatException e){
                    stato.setText("CFU: " + contents.get(position).getCFU());
                    voto = 18;
                    matricola.setText(contents.get(position).getVotoEDataEsameSvolto().substring(0,contents.get(position).getVotoEDataEsameSvolto().indexOf("-")-1));
                }




                return new RecyclerView.ViewHolder(view) {
                };
            }
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case TYPE_HEADER:
                break;
            case TYPE_CELL:
                break;
        }
    }

   public void update(){
       TextView ponderata = (TextView)firstView.findViewById(R.id.ponderataText);
       TextView aritmetica = (TextView)firstView.findViewById(R.id.aritmeticaText);
       ponderata.setText(medie.getPonderata());
       aritmetica.setText(medie.getAritmetica());
       this.notifyDataSetChanged();
   }
}