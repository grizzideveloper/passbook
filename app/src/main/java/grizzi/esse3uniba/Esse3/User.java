package grizzi.esse3uniba.Esse3;

/**
 * Created by Graziano on 03/04/2016.
 */
public class User {//teoricamente c'è un solo utente, non è una brutta idea usare il singleton

    public static class EmptyFieldException extends Exception{
        public EmptyFieldException(){
            super("Password o nick non inseriti");
        }
    }

    private String name = "",
                   password = "",
                   id = "";
    private boolean multipleCarrer  = false;

    public User(String name, String password)throws EmptyFieldException{
            if(!name.isEmpty() && !password.isEmpty()){
                setName(name);
                setPassword(password);
            }else {
                throw new EmptyFieldException();
            }
    }

    public User(String name,String password,String id)throws EmptyFieldException{
        this(name,password);

        if(id != null){
            setId(id);
        }else throw  new EmptyFieldException();
    }

    public User(String name,String password,String id,boolean multipleCarrer)throws EmptyFieldException{
        this(name,password,id);

        setMultipleCarrer(multipleCarrer);
    }

    public boolean isMultipleCarrer() {
        return multipleCarrer;
    }

    public void setMultipleCarrer(boolean multipleCarrer) {
        this.multipleCarrer = multipleCarrer;
    }

    public String getName() {
        return name;
    }

    private void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    private void setPassword(String password) {
        this.password = password;
    }
}
