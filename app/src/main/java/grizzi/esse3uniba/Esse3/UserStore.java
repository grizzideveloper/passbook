package grizzi.esse3uniba.Esse3;

import android.content.Context;
import android.widget.Toast;

import grizzi.esse3uniba.My.MySharedPreferences;

/**
 * Created by Graziano on 03/04/2016.
 */
public class UserStore extends MySharedPreferences {

    private static UserStore instance;
    private Context context;

    private UserStore(Context context){
        super(context,KEY,KEY + "Key",true);
        this.context = context;
    }

    public static class NoUserFound extends Exception{
        public NoUserFound(){
            super("No user found");
        }
    }

    private static final String KEY = "CREDENTIAL";
    private static final String PASSWORD = "PASSWORD";
    private static final String NAME = "NAME";
    private static final String ID = "ID";

    public static UserStore getInstance(Context context){

        if (instance == null) {
            instance = new UserStore(context);
        }
        return instance;
    }

    public void saveUser(User user) { //è giusto a livello di progetto?
        if (user == null) {
            getPreferences().edit().remove(toKey(NAME)).commit();
            getPreferences().edit().remove(toKey(PASSWORD)).commit();
            getPreferences().edit().remove(toKey(ID)).commit();
            getPreferences().edit().remove(toKey("mCarrer")).commit();
        } else {
            putValue(toKey(NAME), user.getName());
            putValue(toKey(PASSWORD), user.getPassword());
            putValue(toKey(ID), user.getId());
            if(user.isMultipleCarrer()){
                putValue(toKey("mCarrer"),"true");
            }else putValue(toKey("mCarrer"),"false");
        }
    }

    public User getSavedUser()throws NoUserFound {
        boolean mCarrer = false;
        if(getString("mCarrer","").contains("true")){
            mCarrer = true;
        }
        try{
            User user = new User(getString(NAME,""),getString(PASSWORD,""),getString(ID,""),mCarrer);
            return user;
        }catch (Exception e){
            throw new NoUserFound();
        }
    }

}
