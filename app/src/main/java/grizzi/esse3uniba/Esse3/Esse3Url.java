package grizzi.esse3uniba.Esse3;

/**
 * Created by Graziano on 20/01/2016.
 */
public final class Esse3Url {

    public static final String LOGIN_URL = "https://www.studenti.ict.uniba.it/esse3/auth/Logon.do";
    public static final String HOME_URL = "https://www.studenti.ict.uniba.it/esse3/auth/Home.do";
    public static final String LIBRETTO_URL = "https://www.studenti.ict.uniba.it/esse3/auth/studente/Libretto/LibrettoHome.do";
    public static final String APPELLI_URL = "https://www.studenti.ict.uniba.it/esse3/auth/studente/Appelli/AppelliF.do";
    public static final String PRENOTAZIONE_URL = "https://www.studenti.ict.uniba.it/esse3/auth/studente/Appelli/DatiPrenotazioneAppello.do";
    public static final String CONFERMA_PRENOTAZIONE_URL = "https://www.studenti.ict.uniba.it/esse3/auth/studente/Appelli/EffettuaPrenotazioneAppello.do";
    public static final String PROVE_URL ="https://www.studenti.ict.uniba.it/esse3/auth/studente/Appelli/AppelliP.do";
    public static final String BACHECA_PRENOTAZIONI = "https://www.studenti.ict.uniba.it/esse3/auth/studente/Appelli/BachecaPrenotazioni.do";
    public static final String CANCELLA_PRENOTAZIONI = "https://www.studenti.ict.uniba.it/esse3/auth/studente/Appelli/CancellaAppello.do";
    public static final String CONFERMA_CANCELLA_PRENOTAZIONE = "https://www.studenti.ict.uniba.it/esse3/auth/studente/Appelli/ConfermaCancellaAppello.do";
    public static final String BACHECA_VOTI = "https://www.studenti.ict.uniba.it/esse3/auth/studente/Appelli/BachecaEsiti.do";
    public static final String GESTIONE_VOTI = "https://www.studenti.ict.uniba.it/esse3/auth/studente/Appelli/GestioneEsiti.do";
    public static final String PAGAMENTI = "https://www.studenti.ict.uniba.it/esse3/auth/studente/Tasse/ListaFatture.do";
}
