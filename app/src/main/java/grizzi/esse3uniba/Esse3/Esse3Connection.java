package grizzi.esse3uniba.Esse3;


import android.os.AsyncTask;
import android.util.Base64;

import android.webkit.WebView;


import grizzi.esse3uniba.My.MyBrowser;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.net.CookieHandler;
import java.net.CookieManager;

import java.net.HttpCookie;

import java.nio.channels.AlreadyConnectedException;

import java.util.List;


import grizzi.esse3uniba.Page.*;

/**
 * Created by Graziano on 19/01/2016.
 */
public class Esse3Connection {//Pattern singleton

    private static Esse3Connection instance;

    private static Boolean connectionState;
    private static String auth;
    private static CookieManager cookieManager;
    private static User user = null;
    private WebView wv;



    private Esse3Connection() {
        setConnectionState(false);
        cookieManager = new CookieManager();
        CookieHandler.setDefault(cookieManager);
    }

    public String getCookie(){
        List<HttpCookie> cookieList = cookieManager.getCookieStore().getCookies();
        System.out.println("Cookie; " + cookieList.get(0).getValue());
        return cookieList.get(0).getValue();
    }

    public WebView getWv() {
        return wv;
    }

    public void setWv(WebView wv) {
        this.wv = wv;
        this.wv.setWebViewClient(new MyBrowser(getNick(),getPassword()));
    }



    public String getId() {
        return user.getId();
    }

    public void setId(String id) {
        Esse3Connection.user.setId(id);
    }

    public static Esse3Connection getInstance(){
        if(instance == null)instance = new Esse3Connection();
        return instance;
    }

    public void close(){
        if(getConnectionState())setConnectionState(false);
    }

    private void setConnectionState(Boolean state) {
        connectionState = state;
    }

    public Boolean getConnectionState() {
        return connectionState;
    }

    public String getAuth() {
        return auth;
    }

    public void setAuth() throws NullPointerException{
        if(user != null){
            auth = makeAuthentication(getNick(),getPassword());
        }else throw new NullPointerException("Riferimento null all'oggetto User");
    }

    public String getNick() {
        return user.getName();
    }


    public String getPassword() {
        return user.getPassword();
    }


    public User getUser() {
        return user;
    }

    protected void setUser(User user) {
        Esse3Connection.user = user;
    }

    private String makeAuthentication(String name, String password) {
        String authentication = Base64.encodeToString((new StringBuilder())
                .append(name)
                .append(":")
                .append(password)
                .toString()
                .getBytes(), 2);
        return authentication;
    }

    public void stopConnection(){
        if(getConnectionState()){
            setConnectionState(false);
            instance = new Esse3Connection();
        }else{
            //errore connessione non trovata

        }
    }

    public void startConnection(User user, LoginListener loginListener) throws AlreadyConnectedException,NullPointerException{

        if (loginListener != null) {

            if (!getConnectionState()) {

                setUser(user);
                setAuth();
                new login(loginListener).execute();

            } else {
                throw new AlreadyConnectedException();
            }
        } else {
            throw new NullPointerException("Riferimento all'adapter è null");
        }
    }

    private class login extends AsyncTask<Void, Void, Void> {

        private LoginListener loginListener;
        private Exception exception;
        private Document document;

        login(LoginListener loginListener){
            this.loginListener = loginListener;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onPostExecute(Void v) {
            if(getConnectionState()){
                loginListener.onLoginDone(document);
            }else{
                loginListener.onLoginFailed(exception);
            }
        }

        @Override
        protected Void doInBackground(Void... arg0) {

            for(int i=0;i<3 && !getConnectionState();i++){
                try {
                    document = pageRequestService(Esse3Url.LOGIN_URL);
                    setConnectionState(true);
                } catch (IOException e) {
                    //alla prima connessione riceverò un 401 per via della mancanza del cookie
                    //la seconda anche ma non so perchè
                }
            }
            return null;
        }


    }


    public void getPage(String url, PageListener pageListener) {
        if (pageListener != null) {
            if (getConnectionState()) {
                new pageRequest(url, pageListener).execute();
            } else {
                //errore
            }
        } else {
            //Errore
        }

    }

    private class pageRequest extends AsyncTask<Void, Void, Boolean> {

        private String url;
        private PageListener pageListener;
        private Document document;
        private Exception exception;

        pageRequest(String url, PageListener pageListener) {
            this.url = url;
            this.pageListener = pageListener;
        }

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected void onPostExecute(Boolean done) {
            if(done){
                pageListener.onPageLoadDone(document);
            }else {
                pageListener.onPageLoadFailed(exception);
            }
        }

        @Override
        protected Boolean doInBackground(Void... arg0) {

            try {

                document = pageRequestService(url);
                return true;

            } catch (IOException e) {
                exception = e;
                return false;
            }

        }
    }

    protected  Document pageRequestService(String url) throws  IOException {

        Document document;
        document = Jsoup.connect(url).header("Authorization", (new StringBuilder()).append("Basic ")
                .append(getAuth()).toString())
                .method(org.jsoup.Connection.Method.GET)
                .timeout(30000)
                .execute()
                .parse();

        return document;
    }


    public void execute(String url,PageListener pageListener){
        new PageRequestTask(url,pageListener).execute(this);
    }
}
