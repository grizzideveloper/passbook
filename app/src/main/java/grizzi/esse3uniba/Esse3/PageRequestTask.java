package grizzi.esse3uniba.Esse3;

import android.os.AsyncTask;

import org.jsoup.nodes.Document;

import java.io.IOException;

import grizzi.esse3uniba.Page.PageListener;

/**
 * Created by Graziano Rizzi on 03/12/2016.
 */

class PageRequestTask extends AsyncTask<Esse3Connection,Void,Boolean> {

    private String url;
    private PageListener pageListener;
    private Document document;
    private Exception exception;

    PageRequestTask(String url, PageListener pageListener) {
        this.url = url;
        this.pageListener = pageListener;
    }

    @Override
    protected void onPreExecute() {

    }

    @Override
    protected void onPostExecute(Boolean done) {
        if(done){
            pageListener.onPageLoadDone(document);
        }else {
            pageListener.onPageLoadFailed(exception);
        }
    }

    @Override
    protected Boolean doInBackground(Esse3Connection... arg0) {

        try {
            document = arg0[0].pageRequestService(url);
            return true;
        } catch (IOException e) {
            exception = e;
            return false;
        }

    }
}
