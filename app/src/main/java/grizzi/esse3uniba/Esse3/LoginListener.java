package grizzi.esse3uniba.Esse3;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.widget.Toast;

import org.jsoup.nodes.Document;

/**
 * Created by Graziano on 28/01/2016.
 */
public abstract class LoginListener {

    public abstract void onLoginFailed(Exception e);

    public abstract void onLoginDone(Document document);

    public void printMsg(final String msg,final Context context, final int length){
        Handler handler =  new Handler(context.getMainLooper());
        handler.post( new Runnable(){
            public void run(){
                Toast.makeText(context, msg, length).show();
            }
        });
    }

}
