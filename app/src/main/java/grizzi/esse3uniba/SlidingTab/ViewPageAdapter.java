package grizzi.esse3uniba.SlidingTab;

/**
 * Created by Graziano on 03/02/2016.
 */
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import grizzi.esse3uniba.Page.PageCalendar;
import grizzi.esse3uniba.Page.Tabs.tab1;
import grizzi.esse3uniba.Page.Tabs.tab2;
import grizzi.esse3uniba.Page.Tabs.tab3;

/**
 * Created by hp1 on 21-01-2015.
 */
public class ViewPageAdapter extends FragmentStatePagerAdapter {

    CharSequence Titles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
    int NumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created


    // Build a Constructor and assign the passed Values to appropriate values in the class
    public ViewPageAdapter(FragmentManager fm,CharSequence mTitles[], int mNumbOfTabsumb) {
        super(fm);

        this.Titles = mTitles;
        this.NumbOfTabs = mNumbOfTabsumb;

    }

    //This method return the fragment for the every position in the View Pager
    @Override
    public Fragment getItem(int position) {

        Fragment tab = new Fragment();

        switch (position){
            case 0:
                tab = new tab2();
                break;
            case 1:
                tab = new tab1();
                break;
            case 2:
                tab = new tab3();
                break;
            case 3:
                tab = new PageCalendar();
        }


        return tab;
    }

    // This method return the titles for the Tabs in the Tab Strip

    @Override
    public CharSequence getPageTitle(int position) {
        return Titles[position];
    }

    // This method return the Number of tabs for the tabs Strip

    @Override
    public int getCount() {
        return NumbOfTabs;
    }
}